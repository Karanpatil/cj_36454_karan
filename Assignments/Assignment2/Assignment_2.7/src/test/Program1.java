package test;

import java.util.Scanner;

public class Program1 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("--------------------------------------");
		System.out.print("Total miles driven per day	:	");
		float miles = sc.nextFloat();

		System.out.println("--------------------------------------");
		System.out.print("Cost per gallon of gasoline	:	");
		float gasolineCost = sc.nextFloat();
		
		System.out.println("--------------------------------------");
		System.out.print("Average miles per gallon	:	");
		float avgMiles = sc.nextFloat();
		
		System.out.println("--------------------------------------");
		System.out.print("Parking fees per day	:	");
		float parkingFees = sc.nextFloat();

		System.out.println("--------------------------------------");
		System.out.print("Toll per day	:	");
		float toll = sc.nextFloat();

		float cost = ((miles/avgMiles) * gasolineCost) + parkingFees + toll;
		
		System.out.println("--------------------------------------");
		System.out.println("Cost per day : " + cost);
				
		sc.close();
	}
}
