package test;

public class Program1 {
	
	public static void sumOfDigits(int number) {
		int num = 0, result = 0;
		while(number != 0) {
			num = number % 10;
			result += num;
			number /= 10;
		}
		System.out.println("Sum of digits : " + result);
	}

	public static void reversNumber(int number) {
		int reverse = 0;

		while(number != 0) {
			reverse = (reverse * 10) + (number % 10);
			number /= 10;
		}
		
		System.out.println("Reverse Number : " + reverse);

	}
	
	public static void palindromeNumber(int number) {
		int temp = number, reverse = 0;
		
		while(number != 0) {
			reverse = (reverse * 10) + (number % 10);
			number /= 10;
		}
		
		if(reverse == temp) {
			System.out.println("Number " + reverse + " is palindrome");
		}
		else {
			System.out.println("Number " + reverse + " is not palindrome");
		}
	}

	public static void perfectNumber(int number) {
		int sum = 0;
		for(int i = 1; i <= number/2; i++) {
			if((number % i) == 0) {
				sum += i;
			}
		}
		
		if(sum == number) {
			System.out.println(number + " is a perfect number");
		}
		else {
			System.out.println(number + " is not a perfect number");
		}
	}

	public static void strongNumber(int number) {
		int temp = number, fact = 1, result = 0;
		while(number != 0) {
			for(int i = 1; i <= (number % 10); i++) {
				fact = fact * i;
			}
			number /= 10;
			result += fact;
			fact = 1;
		}
		if(result == temp) {
			System.out.println(temp + " is a strong number");
		}
		else {
			System.out.println(temp + " is not a strong number");
		}
	}

	public static void armstrongNumber(int number) {
		int sum = 0, temp = number;
		while (number != 0) {
			sum += ((number % 10) * (number % 10) * (number % 10));
			number /= 10;
		}
		
		if(temp == sum) {
			System.out.println(temp + " is Armstrong number");
		}
		else {
			System.out.println(temp + " is not a Armstrong number");
		}
	}

	public static void primeNumber(int number) {
		System.out.println("Prime numbers from 1 to 100 are : ");
		for(int i = 2; i <= number; i++) {
			int count = 0;
			for(int j = 2; j <= i; j++) {
				if(i % j == 0) {
					count++;
				}
			}
			if(count <= 1) {
				System.out.print(i + ", ");
			}
		}
	}

	public static void main(String[] args) {
		int number = Integer.parseInt(args[0]);
		//sumOfDigits(number);
		//reversNumber(number);
		//palindromeNumber(number);
		//perfectNumber(number);
		//strongNumber(number);
		//armstrongNumber(number);
		//primeNumber(number);
		
		
	}
}
