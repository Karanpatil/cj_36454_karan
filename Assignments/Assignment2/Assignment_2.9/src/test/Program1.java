package test;

import java.util.Scanner;

public class Program1 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("First digit	:	");
		int num1 = sc.nextInt();
		
		System.out.print("Second digit	:	");
		int num2 = sc.nextInt();

		System.out.print("Third digit	:	");
		int num3 = sc.nextInt();
		
		int[] arr = { num1, num2, num3 };
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int z = 0; z < 3; z++) {
					if(i != j && j != z && i != z) {
						System.out.print(arr[i] + "" + arr[j] + "" + arr[z] + " ");
					}
				}
			}
		}
		
		
		sc.close();
	}
}
