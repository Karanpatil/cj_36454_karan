package test;

import java.util.Scanner;

public class Program1 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Current world population (Billion)	:	");
		int population = sc.nextInt();
		System.out.println("-----------------------------------------------------------");

		System.out.print("Growth rate	:	");
		float rate = sc.nextFloat();
		System.out.println("-----------------------------------------------------------");
		
		int firstYear = (int) (population * rate);
		System.out.println("Population after one year	:	" + firstYear);
		System.out.println("-----------------------------------------------------------");
		
		int twoYear = (int) (firstYear * rate);
		System.out.println("Population after two year	:	" + twoYear);
		System.out.println("-----------------------------------------------------------");

		int threeYear = (int) (twoYear * rate);
		System.out.println("Population after three year	:	" + threeYear);
		System.out.println("-----------------------------------------------------------");

		int fourYear = (int) (threeYear * rate);
		System.out.println("Population after four year	:	" + fourYear);
		System.out.println("-----------------------------------------------------------");

		int fiveYear = (int) (fourYear * rate);
		System.out.println("Population after five year	:	" + fiveYear);
		System.out.println("-----------------------------------------------------------");

		sc.close();
	}
}
