package test;

import java.util.Scanner;

public class Program1 {
	
	public static void main(String[] args) {
		int result;
		
		Scanner sc =  new Scanner(System.in);

		System.out.println("Enter two numbers to perform operation ");
		int number1 = sc.nextInt();
		int number2 = sc.nextInt();
		
		int choice;
		do {
			System.out.println("Choose option to perform opertion");
			System.out.println("0.Exit");
			System.out.println("1.Addition");
			System.out.println("2.Subtraction");
			System.out.println("3.Multiplication");
			System.out.println("4.Division");
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				//Addition
				result = number1 + number2;
				System.out.println("Addition of " + number1 + " and " + number2 + " is : " + result);
				break;
			case 2:
				//Subtraction
				result = number1 - number2;
				System.out.println("Subtraction of " + number1 + " and " + number2 + " is : " + result);
				break;
			case 3:
				//Multiplication
				result = number1 * number2;
				System.out.println("Multiplication of " + number1 + " and " + number2 + " is : " + result);
				break;
			case 4:
				//Division
				result = number1 / number2;
				System.out.println("Dividion of " + number1 + " and " + number2 + " is : " + result);
				break;
			default:
				break;
			}
			
		} while (choice != 0);
		sc.close();
	}

}
