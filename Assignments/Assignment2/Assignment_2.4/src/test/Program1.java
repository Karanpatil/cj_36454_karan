package test;

public class Program1 {
	
	public static void main(String[] args) {
		int number1 = 9, number2 = 5;
		int result;
		
		//Addition
		result = number1 + number2;
		System.out.println("Addition of " + number1 + " and " + number2 + " is : " + result);

		//Subtraction
		result = number1 - number2;
		System.out.println("Subtraction of " + number1 + " and " + number2 + " is : " + result);

		//Multiplication
		result = number1 * number2;
		System.out.println("Multiplication of " + number1 + " and " + number2 + " is : " + result);

		//Dividion
		result = number1 / number2;
		System.out.println("Dividion of " + number1 + " and " + number2 + " is : " + result);

	}

}
