package test;

import java.util.Scanner;

public class Program1 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		float BMI;
		System.out.println("--------------------------------------");
		System.out.println("BMI Calculator");
		System.out.println("--------------------------------------");
		System.out.print("Enter weight in Kilograms	:	");
		float weight = sc.nextFloat();
		System.out.println("--------------------------------------");
		System.out.println("Enter height in Meters	:	");
		float height = sc.nextFloat();
		System.out.println("--------------------------------------");
		
		BMI = ((weight)/((height)*(height)));
		
		System.out.println("BMI : " + BMI);
		System.out.println("--------------------------------------");
		
		if(BMI < 18.5) {
			System.out.println("Underweight");
		} 
		else if((BMI > 18.5) && (BMI < 24.9)) {
			System.out.println("Normal");
		}
		else if((BMI > 25) &&(BMI < 29.5)) {
			System.out.println("Overweight");
		}
		else if(BMI >= 30) {
			System.out.println("Obese");
		}
		System.out.println("--------------------------------------");
		
		sc.close();
	}
}
