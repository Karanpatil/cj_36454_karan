package test;

import java.util.Scanner;

public class CreditLimitCalculatorTest {
	CreditLimitCalculator creditCalculator = new CreditLimitCalculator(0, 0, 0, 0, 0);
	static Scanner sc = new Scanner(System.in);
	
	public void acceptRecord( ) {
		System.out.print("Account Number : ");
		creditCalculator.setAccountNumber(sc.nextInt());
		
		System.out.print("Balance at beginning of month : ");
		creditCalculator.setBeginningBalance(sc.nextInt());
		
		System.out.println("Items Charger for Month : ");
		creditCalculator.setTotalItems(sc.nextInt());
		
		System.out.println("Credits Applied : ");
		creditCalculator.setCreditsApplied(sc.nextInt());
		
		System.out.println("Allowed Credit limit : ");
		creditCalculator.setAllowedCreditLimit(sc.nextInt());
	}
	public void newBalance( ) {
		int balance = creditCalculator.getBeginningBalance() + creditCalculator.getTotalItems() - creditCalculator.getCreditsApplied();
		
		if( balance < creditCalculator.getAllowedCreditLimit()) {
			System.out.println("Credit Limit Exceeded");
		}else {
			System.out.println("Balance " + balance);

		}
	}

	
	
}
