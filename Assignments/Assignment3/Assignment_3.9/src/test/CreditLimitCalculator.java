package test;

public class CreditLimitCalculator {
	private int accountNumber;
	private int beginningBalance;
	private int totalItems;
	private int creditsApplied;
	private int allowedCreditLimit;
	public CreditLimitCalculator(int accountNumber, int beginningBalance, int totalItems, int creditsApplied,
			int allowedCreditLimit) {
		this.accountNumber = accountNumber;
		this.beginningBalance = beginningBalance;
		this.totalItems = totalItems;
		this.creditsApplied = creditsApplied;
		this.allowedCreditLimit = allowedCreditLimit;
	}
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public int getBeginningBalance() {
		return beginningBalance;
	}
	public void setBeginningBalance(int beginningBalance) {
		this.beginningBalance = beginningBalance;
	}
	public int getTotalItems() {
		return totalItems;
	}
	public void setTotalItems(int totalItems) {
		this.totalItems = totalItems;
	}
	public int getCreditsApplied() {
		return creditsApplied;
	}
	public void setCreditsApplied(int creditsApplied) {
		this.creditsApplied = creditsApplied;
	}
	public int getAllowedCreditLimit() {
		return allowedCreditLimit;
	}
	public void setAllowedCreditLimit(int allowedCreditLimit) {
		this.allowedCreditLimit = allowedCreditLimit;
	}
	
	
	
}
