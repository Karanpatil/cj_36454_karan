package test;

public class Plan {
	private float monthlyCharges;
	private float callCharges;
	private float smsCharges;
	private int freeCalls;
	private int discount50;
	
	public Plan() {
		this(0.0f, 0.0f, 0.0f, 0, 0);
	}
	public Plan(float monthlyCharges, float callCharges, float smsCharges, int freeCalls, int discount50) {
		super();
		this.monthlyCharges = monthlyCharges;
		this.callCharges = callCharges;
		this.smsCharges = smsCharges;
		this.freeCalls = freeCalls;
		this.discount50 = discount50;
	}
	public float getMonthlyCharges() {
		return monthlyCharges;
	}
	public void setMonthlyCharges(float monthlyCharges) {
		this.monthlyCharges = monthlyCharges;
	}
	public float getCallCharges() {
		return callCharges;
	}
	public void setCallCharges(float callCharges) {
		this.callCharges = callCharges;
	}
	public float getSmsCharges() {
		return smsCharges;
	}
	public void setSmsCharges(float smsCharges) {
		this.smsCharges = smsCharges;
	}
	public int getFreeCalls() {
		return freeCalls;
	}
	public void setFreeCalls(int freeCalls) {
		this.freeCalls = freeCalls;
	}
	public int getDiscount50() {
		return discount50;
	}
	public void setDiscount50(int discount50) {
		this.discount50 = discount50;
	}
	
	
}
