package test;

import java.util.Scanner;

public class Program {

	public static void display(Plan planA, Plan planB, Plan planC) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Plan (A/B/C)	:	");
		String choice = sc.nextLine();
		System.out.print("Calls	:	");
		int call = sc.nextInt();
		System.out.print("SMS	:	");
		int sms = sc.nextInt();
		int remainingCalls1;
		System.out.println("Items				Amount				Expanation");
		if(choice.equals("A")) {
			remainingCalls1 = call - planA.getFreeCalls();
			if(remainingCalls1 < 0) {
				System.out.println("First " + call + " calls		Rs. 0			Free calls");
			}
			else {
				if(remainingCalls1 > planA.getDiscount50()) {
					remainingCalls1 -= 50; 
					System.out.println("First 50 calls			Rs. 0			Free calls");
					System.out.println("Next 50 calls			"+ "Rs. " + (50*0.5) + "		50 * 0.5 (50% Discount)");
					System.out.println("Remaining " +remainingCalls1+ " calls		"+ "Rs. " + (remainingCalls1*1.0) + "		"+remainingCalls1+" * 1.0 (Regular Billing)");
					System.out.println("SMS Bill			"+ "Rs. " + (sms*1.0) + "		"+sms+" * 1.0 (Regular Billing)");
					System.out.println("Total Charges			"+ "Rs. " + ((50*0.5)+(remainingCalls1*1.0)+(sms*1.0)) + "		 Monthly charges + Bill charges");
					double taxBill = (50*0.5)+(remainingCalls1*1.0)+(sms*1.0);
					System.out.println("Final Bill			"+ "Rs. " + (taxBill +(taxBill * 0.125)) + "		 Total charges + Service Tax");
					
				}
				else {
					System.out.println("First 50 calls			Rs. 0			Free calls");
					System.out.println("Next " + remainingCalls1 +" calls			"+ "Rs. " + (remainingCalls1*0.5) + "		" + remainingCalls1+ " * 0.5 (50% Discount)");
					System.out.println("SMS Bill			"+ "Rs. " + (sms*1.0) + "		"+sms+" * 1.0 (Regular Billing)");
					System.out.println("Total Charges			"+ "Rs. " + ((remainingCalls1*0.5)+(sms*1.0)) + "		 Monthly charges + Bill charges");
					double taxbill = ((remainingCalls1*0.5)+(sms*1.0));
					System.out.println("Final Bill			"+ "Rs. " + (taxbill + (taxbill * 0.125)) + "		 Total charges + Service Tax");
					
				}
			}
			
		}
		else if(choice.equals("B")) {
			remainingCalls1 = call - planB.getFreeCalls();
			if(remainingCalls1 < 0) {
				System.out.println("First " + call + " calls		Rs. 0			Free calls");
			}
			else {
				if(remainingCalls1 > planB.getDiscount50()) {
					remainingCalls1 -= 75; 
					System.out.println("First 75 calls			Rs. 0			Free calls");
					System.out.println("Next 75 calls			"+ "Rs. " + (75*0.4) + "		75 * 0.5 (50% Discount)");
					System.out.println("Remaining " +remainingCalls1+ " calls		"+ "Rs. " + (remainingCalls1*0.8) + "		"+remainingCalls1+" * 1.0 (Regular Billing)");
					System.out.println("SMS Bill			"+ "Rs. " + (sms*0.75) + "		"+sms+" * 0.75 (Regular Billing)");
					System.out.println("Total Charges			"+ "Rs. " + ((75*0.5)+(remainingCalls1*0.8)+(sms*0.75)) + "		 Monthly charges + Bill charges");
					double taxBill = (75*0.5)+(remainingCalls1*0.8)+(sms*0.75);
					System.out.println("Final Bill			"+ "Rs. " + (taxBill +(taxBill * 0.125)) + "		 Total charges + Service Tax");
					
				}
				else {
					System.out.println("First 50 calls			Rs. 0			Free calls");
					System.out.println("Next " + remainingCalls1 +" calls			"+ "Rs. " + (remainingCalls1*0.4) + "		" + remainingCalls1+ " * 0.5 (50% Discount)");
					System.out.println("SMS Bill			"+ "Rs. " + (sms*0.75) + "		"+sms+" * 0.75 (Regular Billing)");
					System.out.println("Total Charges			"+ "Rs. " + ((remainingCalls1*0.4)+(sms*0.75)) + "		 Monthly charges + Bill charges");
					double taxbill = ((remainingCalls1*0.4)+(sms*0.75));
					System.out.println("Final Bill			"+ "Rs. " + (taxbill + (taxbill * 0.125)) + "		 Total charges + Service Tax");
					
				}
			}
			
		}
		else if(choice.equals("C")) {
			remainingCalls1 = call - planC.getFreeCalls();
			if(remainingCalls1 < 0) {
				System.out.println("First " + call + " calls		Rs. 0			Free calls");
			}
			else {
				if(remainingCalls1 > planC.getDiscount50()) {
					remainingCalls1 -= 75; 
					System.out.println("First 100 calls			Rs. 0			Free calls");
					System.out.println("Next 100 calls			"+ "Rs. " + (100*0.3) + "		100 * 0.3 (50% Discount)");
					System.out.println("Remaining " +remainingCalls1+ " calls		"+ "Rs. " + (remainingCalls1*0.6) + "		"+remainingCalls1+" * 1.0 (Regular Billing)");
					System.out.println("SMS Bill			"+ "Rs. " + (sms*0.5) + "		"+sms+" * 0.5 (Regular Billing)");
					System.out.println("Total Charges			"+ "Rs. " + ((100*0.3)+(remainingCalls1*0.6)+(sms*0.5)) + "		 Monthly charges + Bill charges");
					double taxBill = (100*0.3)+(remainingCalls1*0.6)+(sms*0.5);
					System.out.println("Final Bill			"+ "Rs. " + (taxBill +(taxBill * 0.125)) + "		 Total charges + Service Tax");
					
				}
				else {
					System.out.println("First 100 calls			Rs. 0			Free calls");
					System.out.println("Next " + remainingCalls1 +" calls			"+ "Rs. " + (remainingCalls1*0.6) + "		" + remainingCalls1+ " * 0.5 (50% Discount)");
					System.out.println("SMS Bill			"+ "Rs. " + (sms*0.5) + "		"+sms+" * 0.5 (Regular Billing)");
					System.out.println("Total Charges			"+ "Rs. " + ((remainingCalls1*0.3)+(sms*0.5)) + "		 Monthly charges + Bill charges");
					double taxbill = ((remainingCalls1*0.3)+(sms*0.5));
					System.out.println("Final Bill			"+ "Rs. " + (taxbill + (taxbill * 0.125)) + "		 Total charges + Service Tax");
					
				}
			}
			
		}
		
	}
	
	public static void main(String[] args) {
		Plan planA = new Plan(199, 1.00f, 1.00f, 50, 50);
		Plan planB = new Plan(299, 0.80f, 0.75f, 75, 75);
		Plan planC = new Plan(399, 0.60f, 0.50f, 100, 100);
		
		Program.display(planA, planB, planC);
	}

}
