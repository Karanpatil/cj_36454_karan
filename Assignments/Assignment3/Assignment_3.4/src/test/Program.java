package test;

public class Program {
	
	public static int personsAge(HeartRate hr) {
		int age = 2020 - (hr.getDate().getYear());
		return age;
	}
	
	public static int maximumHeartRate(HeartRate hr) {
		int max = 220 - (2020 - (hr.getDate().getYear()));
		return max;
	}
	
	public static String targetHeartRate(HeartRate hr) {
		int target1 = (int) ((220 - (2020 - (hr.getDate().getYear()))) * 0.5);
		int target2 = (int) ((220 - (2020 -  (hr.getDate().getYear()))) * 0.85);
		String target = (target1 + " - " + target2);
		return target;
	}

	public static void main(String[] args) {
		Date date = new Date(12,8,1996);
		HeartRate hr = new HeartRate("abc", "qqq", date);
		
		System.out.println("Current age is : " + Program.personsAge(hr) + " years");
		
		System.out.println("Maximum heart rate : " + Program.maximumHeartRate(hr) + " beats per minute");

		System.out.println("Target heart rate range : " + Program.targetHeartRate(hr) + " beats per minute");
	}

}
