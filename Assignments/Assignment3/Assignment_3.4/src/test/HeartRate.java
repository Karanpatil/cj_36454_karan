package test;

public class HeartRate {
	private String firstName;
	private String lastName;
	private  Date date;
	
	public HeartRate() {
		this.firstName = "";
		this.lastName = "";
		this.date = new Date();
		this.date.setDay(1);
		this.date.setMonth(1);
		this.date.setYear(1);
	}

	public HeartRate(String firstName, String lastName, Date date) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.date = new Date();
		this.date.setDay(date.getDay());
		this.date.setMonth(date.getMonth());
		this.date.setYear(date.getYear());
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
