package test;

import java.util.Scanner;

public class HealthProfileTest {
	HealthProfile healthProfile = new HealthProfile(null, null, null, 0, 0, 0, 0, 0);
	static Scanner sc = new Scanner(System.in);
	public void acceptRecord( ) {
		System.out.print("First Name : ");
		healthProfile.setFirstName(sc.nextLine());
		
		System.out.print("Last Name : ");
		healthProfile.setLastName(sc.nextLine());
		
		System.out.print("Gender : ");
		healthProfile.setGender(sc.nextLine());
		
		System.out.print("Weight(in pounds) : ");
		healthProfile.setWeight(sc.nextDouble());
		
		System.out.print("Height( in inches) : ");
		healthProfile.setHeight(sc.nextDouble());
		
		System.out.println("Enter Birth Date");
		System.out.print("Day: ");
		healthProfile.setDay(sc.nextInt());
		
		System.out.print("Month : ");
		healthProfile.setMonth(sc.nextInt());
		
		
		System.out.print("Year : ");
		healthProfile.setYear(sc.nextInt());
	}
	public int getAge( ) {
		return 2020 - healthProfile.getYear();
	}
	public int getMaxHeartRates( ) {
		return 220 - this.getAge();
	}
	
	public void getTargetHeartrate( ) {
		System.out.println("Target Heart rate : "+ this.getMaxHeartRates() * 0.5 + " to " + this.getMaxHeartRates() * 0.85);
	} 
	public void BMI( ) {
		double BMI = healthProfile.getWeight() / (healthProfile.getHeight() * healthProfile.getHeight());
		//System.out.println("BMI : " + BMI);
		
		if(BMI < 18.5) {
			System.out.println(" UnderWeight");
		}
		else if(BMI <= 24.9  && BMI >= 18.5) {
			System.out.println("Normal");
		}
		else if(BMI <= 25  && BMI >= 29.9) {
			System.out.println(" OverWeight");
		}
		else if(BMI >= 30) {
			System.out.println("Obese");
		}
	}
	public void printRecord( ) {
		System.out.println("-----Person Details----------");
		System.out.println("First Name : " + this.healthProfile.getFirstName());
		
		System.out.println("Last Name : " + this.healthProfile.getLastName());
		
		System.out.println("Gender : " + this.healthProfile.getGender());

		
		System.out.println("Birth Date : " + this.healthProfile.getDay() + " / " + this.healthProfile.getMonth() + " / " + this.healthProfile.getYear());
		
		System.out.println("Height : " + this.healthProfile.getHeight());
		
		System.out.println("Weight : " + this.healthProfile.getWeight());


		System.out.println("Age : " + this.getAge());
		
		System.out.print("BMI : ");
		this.BMI();
		
		System.out.println("--------Heart Condition------");
		System.out.println("Max Heart rate : " + this.getMaxHeartRates());
		System.out.println("Target heart rate : ");
		this.getTargetHeartrate();
		
	}
}
