package test;

import java.util.Scanner;

public class CircleTest {
	Circle circle = new Circle();
	static Scanner sc = new Scanner(System.in);
	public void acceptRecord( ) {
		System.out.print("Radius : ");
		circle.setRadius(sc.nextInt());
	}
	public double getdiameter() {
		double diameter = circle.getRadius() * 2;
		return diameter;
	}
	public double getCircumference( ) {
		double circumference =2 * Math.PI *  circle.getRadius();
		return circumference;
	}
	
	public double area( ) {
		return circle.getRadius() * circle.getRadius() * 2;
	}
	public void getDetalis( ) {
		System.out.println("Diameter : " + this.getdiameter());
		System.out.println("Circumference " + this.getCircumference());
		System.out.println("Area : " + this.area());
	}
}
