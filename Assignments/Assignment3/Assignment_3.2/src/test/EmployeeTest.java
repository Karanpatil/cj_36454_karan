package test;

import java.util.Scanner;

public class EmployeeTest {
	private Employee[] emp =  new Employee[2];
	static Scanner sc = new Scanner(System.in);
	
	public void acceptRecord() {
		for(int i = 0; i < this.emp.length; i++) {
			this.emp[i] = new Employee("", "", 0.0);
			System.out.println("Employee " + (i+1) + " details");
			sc.nextLine();
			System.out.print("First name	:	");
			String first = sc.nextLine();
			this.emp[i].setFirstName(first);
			System.out.print("Last name	:	");
			String last = sc.nextLine();
			this.emp[i].setLastName(last);
			System.out.print("Salary	:	");
			this.emp[i].setSalary(sc.nextDouble());
		}
	}
	
	public void displaySalary() {
		for(int i = 0; i < this.emp.length; i++) {
			System.out.println("Employee " + (i+1) + " yearly salary");
			System.out.println("Salary	:	" + (this.emp[i].getSalary() * 12));
		}
	}
	
	public void salaryHike() {
		for(int i = 0; i < this.emp.length; i++) {
			this.emp[i].setSalary((this.emp[i].getSalary() * 1.1));
		}
	}
		
}

