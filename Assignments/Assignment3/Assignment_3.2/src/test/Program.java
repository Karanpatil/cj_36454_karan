package test;


public class Program {

	public static void main(String[] args) {
		EmployeeTest test = new EmployeeTest();
		test.acceptRecord();
		test.displaySalary();
		test.salaryHike();
		System.out.println("After Hike");
		test.displaySalary();
	}

}
