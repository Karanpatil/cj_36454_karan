package test;

import java.util.Scanner;

public class InvoiceTest {
	private Invoice invoice = new Invoice();
	static Scanner sc = new Scanner(System.in);
	
	public void acceptRecord(Invoice invoice) {
		
		System.out.println("-------------------Part Daetails-------------------");

		System.out.print("Part Number			:	");
		this.invoice.setPartNumber(sc.nextLine());
		
		System.out.print("Part Description		:	");
		this.invoice.setPartDescription(sc.nextLine());
		
		System.out.print("Quantity to be purchased	:	");
		this.invoice.setQuantity(sc.nextInt());
		
		System.out.print("Price per part			:	");
		this.invoice.setPrice(sc.nextDouble());		
	}
	
	public void generateInvoice(Invoice invoice) {
		System.out.println("---------------------------Invoice---------------------------");
		System.out.println("Part Number			:	" + this.invoice.getPartNumber());
		System.out.println("Part Description		:	" + this.invoice.getPartDescription());
		System.out.println("Part quantity to be purchased	:	" + this.invoice.getQuantity());
		System.out.println("Price per part			:	" + this.invoice.getPrice());
		System.out.println("Total Price			:	" + this.invoice.getInvoiceAmount(this.invoice.getQuantity(), this.invoice.getPrice()));
	}
}
