package test;

public class Program {


	public static void main(String[] args) {
		
		InvoiceTest test = new InvoiceTest();
		Invoice invoice = new Invoice("", "", 0, 0.00);
		
		test.acceptRecord(invoice);
		test.generateInvoice(invoice);
	}

}
