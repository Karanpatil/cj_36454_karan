package test;

import java.util.Scanner;

public class SalesCommissionCalculatortest {
	private SalesCommissionCalculator calculator1 = new SalesCommissionCalculator(0, 0.0, 0);
	private SalesCommissionCalculator calculator2 = new SalesCommissionCalculator(0, 0.0, 0);

	private SalesCommissionCalculator calculator3 = new SalesCommissionCalculator(0, 0.0, 0);

	private SalesCommissionCalculator calculator4 = new SalesCommissionCalculator(0, 0.0, 0);

	static Scanner sc = new Scanner(System.in);
	//int item1, item2, item3, item4;
	public void displayValues( ) {
		System.out.println("Item		Value");
		System.out.println("1		239.99");
		System.out.println("2		129.75");
		System.out.println("3		99.95");
		System.out.println("4		350.89");
	}
	public void acceptRecord( ) {
		System.out.print("Item 1 sold : ");
		calculator1.setCount(sc.nextInt());
		calculator1.setItem(1);
		calculator1.setValue(239.99);
		
		System.out.print("Item 2 sold : ");
		calculator2.setCount(sc.nextInt());
		calculator2.setItem(2);
		calculator2.setValue(129.75);
		
		
		System.out.print("Item 3 sold : ");
		calculator3.setCount(sc.nextInt());
		calculator3.setItem(3);
		calculator3.setValue(99.95);
		
		System.out.print("Item 4 sold : ");
		calculator4.setCount(sc.nextInt());
		calculator4.setItem(3);
		calculator4.setValue(350.89);
		
	}
	public double calculateTotalAmount( ) {
		double total = (calculator1.getCount() * calculator1.getValue())
				+ (calculator2.getCount() * calculator2.getValue())
				+ (calculator3.getCount() * calculator3.getValue())
				+ (calculator4.getCount() * calculator4.getValue());
		return total;
	}
	public double calculateCommission( ) {
		double commission = 200 + (this.calculateTotalAmount() * 0.09);
		return commission;
	}
}
