package test;

public class SalesCommissionCalculator {
	private int item;
	private double value;
	private int count;
	public SalesCommissionCalculator(int item, double value, int count) {
		this.item = item;
		this.value = value;
		this.count = count;
	}
	public int getItem() {
		return item;
	}
	public void setItem(int item) {
		this.item = item;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
}
