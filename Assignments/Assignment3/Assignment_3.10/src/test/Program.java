package test;

public class Program {
	public static void main(String[] args) {
		SalesCommissionCalculatortest test = new SalesCommissionCalculatortest();
		test.displayValues();
		test.acceptRecord();
		System.out.println("-------------------------------------------");
		System.out.println("Total sale 		: "+ test.calculateTotalAmount());
		System.out.println("Salesperson's Earning   : " + test.calculateCommission());
		System.out.println("-------------------------------------------");

	}
}
