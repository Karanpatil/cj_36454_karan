package test;

import java.util.Scanner;
import java.lang.Math;

public class Program {

	public static void acceptRecord(Bank bank) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Deposit	:	");
		bank.setDeposit(sc.nextFloat());

		System.out.println("Year	:	");
		bank.setYear(sc.nextInt());
	}
	
	public static float calculateInterest(Bank bank) {
		float interest = 0.0f, deposit = bank.getDeposit();
		int year = bank.getYear();
		double rate;
		if(year < 5) {
			if((deposit < 2000) && (year > 2)) {
				rate = 5;
			} 
			else if(((deposit >= 2000) && (deposit < 6000)) && (year >= 2)) {
				rate = 7;
			}
			else if((deposit > 6000) && (year >= 1)) {
				rate = 8;
			}
			else {
				rate = 3;
			}
		}
		else {
			rate = 10;
		}
		double cons = 1 + (rate / 100);
		double com = Math.pow(cons, year);
		interest = (float) (deposit * com);
		return interest;
	}
	
	public static void main(String[] args) {
		Bank bank = new Bank(1000, 1);
		Program.acceptRecord(bank);
		float ci = Program.calculateInterest(bank);
		System.out.println("Total balance after " + bank.getYear() + " year is Rs. " + ci);
	}

}
