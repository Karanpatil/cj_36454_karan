package test;

public class Bank {
	private float deposit;
	private int year;
	
	public Bank() {
		this(0.0f, 0);
	}

	public Bank(float deposit, int year) {
		this.deposit = deposit;
		this.year = year;
	}

	public float getDeposit() {
		return deposit;
	}

	public void setDeposit(float deposit) {
		this.deposit = deposit;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	
}
