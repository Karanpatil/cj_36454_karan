package test;

public class Numeric {
	public static void main(String[] args) {
		String a = args[0];
		int number = Integer.parseInt(a);
		System.out.println("Given number : " + a);
		System.out.println("Binary eqivalent : " + Integer.toBinaryString(number));
		System.out.println("Octal eqivalent : " + Integer.toHexString(number));
		System.out.println("Hexadecimal eqivalent : " + Integer.toOctalString(number));		
	}
}
