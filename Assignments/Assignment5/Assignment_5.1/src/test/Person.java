package test;

public class Person {
	private String name;
	private Date joinDate;
	private Address currentAddress;

	public Person() {
		this.name = new String();
		this.joinDate = new Date();
		this.currentAddress = new Address();
	}
	
	public Person(String name, Date joinDate, Address currentAddress) {
		this.name = name;
		this.joinDate = joinDate;
		this.currentAddress = currentAddress;
	}
		
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getJoinDate() {
		return joinDate;
	}
	
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	
	public Address getCurrentAddress() {
		return currentAddress;
	}
	
	public void setCurrentAddress(Address currentAddress) {
		this.currentAddress = currentAddress;
	}
	
	@Override
	public String toString() {
		return "Person name : " + name + ", Joining Date : " + joinDate + ", Current Address : " + currentAddress;
	}
}
