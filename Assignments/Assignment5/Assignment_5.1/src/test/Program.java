package test;

public class Program {

	public static void main(String[] args) {
		Date date = new Date();
		Address address = new Address("Kolhapur", "Maharashtra", 416007);
		Person p = new Person("Dnyanesh", date, address);
		System.out.println(p.toString());
	}
}
