package test;

public class Program {

	public static void main(String[] args) {
		int[] arr = new int[] { 1, 2, 3, 4 };
		Program.displayArrays(arr);
	}

	private static void displayArrays(int[] arr) {
		for (int i = (arr.length - 1); i >= 0; i--) {
			for (int j = i; j >= 0; j--) {
				System.out.print(arr[j] + " ");
			}
			System.out.println();
		}		
	}
}
