package test;

import java.util.Scanner;

public class Program {
	private static double rupees;
	private static Scanner sc = new Scanner(System.in);
	
	public static void toUSD(double currency) {
		System.out.println("Rs." + currency * 74.297);
	}
	
	public static void toEuro(double currency) {
		System.out.println("Rs." + currency * 86.72);
	}

	public static void toAusDollar(double currency) {
		System.out.println("Rs." + currency * 52.251);
	}

	public static void toNZDollar(double currency) {
		System.out.println("Rs." + currency * 49.211);
	}

	public static void toRussDollar(double currency) {
		System.out.println("Rs." + currency * 0.94);
	}

	public static int menuList() {
		int choice;
		System.out.println("Foreign Exchange");
		System.out.println("0. Exit");
		System.out.println("1. US Dollar");
		System.out.println("2. Euros");
		System.out.println("3. Austrailian Dollar");
		System.out.println("4. New Zealand Dollar");
		System.out.println("5. Russian Rouble");
		System.out.print("Choice : ");
		choice = Program.sc.nextInt();
		return choice;
	}
	
	public static void main(String[] args) {
		int choice;
		double currency;
		
		while ((choice = Program.menuList()) != 0) {
			switch (choice) {
			case 1:
				System.out.print("Currency : ");
				currency = Program.sc.nextDouble();
				Program.toUSD(currency);
				break;
			case 2:
				System.out.print("Currency : ");
				currency = Program.sc.nextDouble();
				Program.toEuro(currency);
				break;
			case 3:
				System.out.print("Currency : ");
				currency = Program.sc.nextDouble();
				Program.toAusDollar(currency);
				break;
			case 4:
				System.out.print("Currency : ");
				currency = Program.sc.nextDouble();
				Program.toNZDollar(currency);
				break;
			case 5:
				System.out.print("Currency : ");
				currency = Program.sc.nextDouble();
				Program.toRussDollar(currency);
				break;
			default:
				break;
			}
		}
	}
}
