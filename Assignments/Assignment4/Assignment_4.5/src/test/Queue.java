package test;

public class Queue {
	private static int front;
	private static int rear;
	private static int capacity;
	private static int arr[];
	
	public Queue(int capacity) {
		Queue.front = Queue.rear = 0;
		Queue.capacity = capacity;
		Queue.arr = new int[capacity]; 
	}
	
	public static void enqueue(int number) {
		if(Queue.capacity == Queue.rear) {
			System.out.println("Queue is full");
		}
		else {
			Queue.arr[rear] = number;
			Queue.rear++;
		}
	}

	public static void dequeue() {
		if(Queue.front == Queue.rear) {
			System.out.println("Queue is empty");
		}
		else {
			if(Queue.rear < Queue.capacity)
				Queue.arr[Queue.rear] = 0;
			Queue.rear--;
		}
	}

	public static void displayDequeue() {
		if(Queue.front == Queue.rear) {
			System.out.println("Queue is empty");
		}
		else {
			for (int i = Queue.front; i < Queue.rear; i++) {
				System.out.print(Queue.arr[i] + " ");
			}
			System.out.println();
		}
	}

	public static void queueFront() {
		if(Queue.front == Queue.rear) {
			System.out.println("Queue is empty");
		}
		else {
			System.out.println("Front element is " + Queue.arr[Queue.front]);
		}
	}

	public static void queueEnd() {
		if(Queue.front == Queue.rear) {
			System.out.println("Queue is empty");
		}
		else {
			System.out.println("End element is " + Queue.arr[Queue.rear-1]);
		}
	}
}
