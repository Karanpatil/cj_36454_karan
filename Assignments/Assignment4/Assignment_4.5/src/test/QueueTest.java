package test;

import java.util.Scanner;

public class QueueTest {
	private Scanner sc = new Scanner(System.in);
	private Queue queue;
	
	public int menuList() {
		int choice;
		System.out.println("0. Exit");
		System.out.println("1. Enqueue");
		System.out.println("2. Dequeue");
		System.out.println("3. Queue Front");
		System.out.println("4. Queue End");
		System.out.println("5. Display Queue");
		System.out.print("Select choice : ");
		choice = sc.nextInt();
		return choice;
	}
	
	public void program() {
		int choice, size, number;
		System.out.print("Size of Queue	:	");
		size = this.sc.nextInt();
		this.queue = new Queue(size);
		
		while((choice = this.menuList()) != 0) {
			switch (choice) {
			case 1:
				System.out.print("Elment to be queued : ");
				number = this.sc.nextInt();
				Queue.enqueue(number);
				System.out.println("Element Queued");
				break;
			case 2:
				Queue.dequeue();
				System.out.println("Element Dequed");
				break;
			case 3:
				Queue.queueFront();
				break;
			case 4:
				Queue.queueEnd();
				break;
			case 5:
				Queue.displayDequeue();
				break;
			default:
				break;
			}
		}
	}

}
