package test;

import java.util.Scanner;

public class SavingsAcoountTest {
	private SavingsAccount saver1;
	private SavingsAccount saver2;
	
	public void saverAccount() {
		float interest;
		this.saver1 = new SavingsAccount(2000);
		this.saver2 = new SavingsAccount(3000);
		System.out.println("Saver1 balance : $ 2000");
		System.out.println("Saver2 balance : $ 3000");
		
		SavingsAccount.modifyInterestRate(4);
		System.out.println("For annual interest 4%");
		
		interest = this.saver1.calculateMonthlyInterest();
		System.out.println("Saver1 balance : $ " + (2000 + interest));
		interest = this.saver2.calculateMonthlyInterest();
		System.out.println("Saver2 balance : $ " + (3000 + interest));

		SavingsAccount.modifyInterestRate(5);
		System.out.println("For annual interest 5%");
		
		interest = this.saver1.calculateMonthlyInterest();
		System.out.println("Saver1 balance : $ " + (2000 + interest));
		interest = this.saver2.calculateMonthlyInterest();
		System.out.println("Saver2 balance : $ " + (3000 + interest));
	}
}
