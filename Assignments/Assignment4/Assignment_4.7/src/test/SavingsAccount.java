package test;

public class SavingsAccount {
	private static float annualInterestRate;
	private float savingsBalance;
	
	static {
		SavingsAccount.annualInterestRate = 0.0f;
	}
	
	public SavingsAccount(float balance) {
		this.savingsBalance = balance;
	}

	public static float getAnnualInterestRate() {
		return annualInterestRate;
	}

	public static void modifyInterestRate(float annualInterestRate) {
		SavingsAccount.annualInterestRate = annualInterestRate;
	}

	public float getSavingsBalance() {
		return savingsBalance;
	}

	public void setSavingsBalance(float savingsBalance) {
		this.savingsBalance = savingsBalance;
	}
	
	public float calculateMonthlyInterest() {
		return ((this.savingsBalance * SavingsAccount.annualInterestRate) / 12);
	}
	
}
