package test;

import java.util.Scanner;

public class StackTest {
	private Stack stack;
	private Scanner sc = new Scanner(System.in);
	public int menuList() {
		int choice;
		System.out.println("0. Exit");
		System.out.println("1. Push");
		System.out.println("2. Pop");
		System.out.println("3. Peek");
		System.out.println("4. Display stack");
		System.out.println("5. Check if it is Empty");
		System.out.println("6. Check if it is Full");
		System.out.print("Select choice : ");
		choice = sc.nextInt();
		return choice;
	}
	
	public void program() {
		int choice, size, number;
		boolean status;
		System.out.print("Size of Stack	:	");
		size = this.sc.nextInt();
		this.stack = new Stack(size);
		
		while((choice = this.menuList()) != 0) {
			switch (choice) {
			case 1:
				System.out.print("Elment to push : ");
				number = this.sc.nextInt();
				this.stack.push(number);
				System.out.println("Element pushed");
				break;
			case 2:
				number = this.stack.pop();
				System.out.println("Element popped : " + number);
				break;
			case 3:
				this.stack.peek();
				break;
			case 4:
				System.out.println("Stack : ");
				this.stack.displayStack();
				break;
			case 5:
				status = this.stack.isEmpty();
				if(status)
					System.out.println("Stack is empty");
				else
					System.out.println("Stack is not empty");
				break;
			case 6:
				status = this.stack.isFull();
				if(status)
					System.out.println("Stack is full");
				else
					System.out.println("Stack is not full");
				break;
			default:
				break;
			}
		}
	}
}
