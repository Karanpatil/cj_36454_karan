package test;

public class Stack {
	private int maxSize;
	private int[] arr;
	private static int top;
	
	static {
		Stack.top = -1;
	}
	public Stack(int size) {
		this.maxSize = size;
		this.arr = new int[maxSize];
	}
	
	public void push(int number) {
		if(this.arr[this.maxSize - 1] != 0) 
			System.out.println("Stack is full");
		else {
			this.arr[++Stack.top] = number;
			System.out.println("Element pushed : " + this.arr[Stack.top]);
		}
	}

	public int pop() {
		if(this.arr[0] == 0) 
			System.out.println("Stack is empty");
		else {
			this.arr[Stack.top] = 0;
			return this.arr[--Stack.top];
		}
		return 0;
	}

	public void peek() {
		if(this.arr[0] != 0) {
			int number = this.arr[Stack.top];
			System.out.println("Element peeked : " + number);
		}
		else 
			System.out.println("Stack is empty");
	}
	
	public void displayStack() {
		for (int i = 0; i < this.arr.length; i++) {
			System.out.print(this.arr[i] + " ");
		}
		System.out.println();
	}
	
	public boolean isEmpty() {
		return (Stack.top == -1);
	}

	public boolean isFull() {
		return (Stack.top == (this.maxSize - 1));
	}

}
