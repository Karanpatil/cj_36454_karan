package test;

import java.util.Scanner;

public class TraficLightTest {
	private Scanner sc = new Scanner(System.in);
	
	public int menuList() {
		int choice = 0;
		System.out.println("Traffic Light");
		System.out.println("0. Exit");
		System.out.println("1. Duration");
		System.out.print("Choice	:	");
		choice = sc.nextInt();
		return choice;
	}

	
	
	public void traffic() {
		int choice, d1, d2, d3;
		
		while((choice = this.menuList()) != 0) {
			switch (choice) {
			case 1:
				System.out.print("Duration for RED	:	");
				d1 = this.sc.nextInt();
				System.out.print("Duration for GREEN	:	");
				d2 = this.sc.nextInt();
				System.out.print("Duration for YELLOW	:	");
				d3 = this.sc.nextInt();
				this.displayEnum(d1, d2, d3);
				break;
			default:
				break;
			}
		}
	}



	private void displayEnum(int d1, int d2, int d3) {
		TrafficLight t1 = TrafficLight.RED;
		System.out.println("Enum constant	:	" + t1);
		System.out.println("Duration of " + t1 + "	:	" + d1);
		TrafficLight t2 = TrafficLight.GREEN;
		System.out.println("Enum constant	:	" + t2);
		System.out.println("Duration of " + t2 + "	:	" + d2);
		TrafficLight t3 = TrafficLight.YELLOW;
		System.out.println("Enum constant	:	" + t3);
		System.out.println("Duration of " + t3 + "	:	" + d3);
	}
}
