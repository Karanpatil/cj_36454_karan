package test;

import java.util.Scanner;

public class Program {
    
    static Scanner sc=new Scanner(System.in);
    public static void main(String[] args) {
    
     double[][] sales=new double[5][4];
     
     System.out.print("Enter Sales Number(-1 to end)  :  ");
     int salesNum=sc.nextInt();
     
     while(salesNum!=0) {
        System.out.print("Enter the product number: "); 
        int product = sc.nextInt();
        
        System.out.print("Please enter the sales amount: "); 
        double salesAmount = sc.nextDouble();
        
        if((salesNum < 5) && (salesNum >= 1) && (salesAmount > 0) && (product >= 1) && (product < 6)){
            sales[product-1][salesNum-1]+=salesAmount;
        }
        else {
            System.out.println("Invalid Sales Num");
        }
        
        System.out.print("Enter sales person number(-1 to end):"); 
        product = sc.nextInt(); 
        if(product == -1){
            break;
        }
     }
     
     double personTotal[] = new double[5];
     
     for ( int col = 0; col < 5; col++)
        personTotal[col] = 0;
        
     System.out.printf("%7s%14s%14s%14s%14s%14s\n", "Product", "Salesperson 1",
        "Salesperson 2", "Salesperson 3", "Salesperson 4","Total");
    
     for ( int row = 0; row < 5; row++) {
        double productTotal = 0.0;
        System.out.printf("%7d", (row + 1));
        
        for (int col = 0; col < 4; col++) {
           System.out.printf("%14.2f", sales[row][col]);
            productTotal += sales[row][col];
            personTotal[col] += sales[row][col];
        }
        System.out.printf("%14.2f\n", productTotal);
     }
     
     System.out.printf("%7s", "Total");
     
     for (int col = 0; col < 4; col++)
        System.out.printf("%14.2f", personTotal[col]);
     
     System.out.println();
  
    }
}



