package test;

import java.util.Scanner;

public class Program {
	public static Scanner sc = new Scanner(System.in);
	public static int bookFirstClass(boolean[] airline) {
		int number = -1;
		for (int index = 0; index < (airline.length/2); index++) {
			if (airline[index] != true) {
				airline[index] = true;
				number = index;
				break;
			}
		}
		return number;
	}
	
	public static int bookEconomy(boolean[] airline) {
		int number = -1;
		for (int index = (airline.length/2); index < airline.length; index++) {
			if (airline[index] != true) {
				airline[index] = true;
				number = index;
				break;
			}
		}
		return number;
	}

	public static void displayBoardingPass(int number, String booking) {
		System.out.println("-------Sunbeam Airlines Boarding Pass-------");
		System.out.println("Seat Booked		:	" + booking);
		System.out.println("Seat Number		:	" + (number + 1));
	}
	
	public static int menuList() {
		int choice;
		System.out.println("Welcome to the Sunbeam Airlines \nChoose appropriate option for reservation");
		System.out.println("0. Exit");
		System.out.println("1. First Class");
		System.out.println("2. Economy");
		choice = sc.nextInt();
		return choice;
	}
	
	public static void main(String[] args) {
		int choice, number;
		boolean[] airline = new boolean[10];
		
		while((choice = Program.menuList()) != 0) {
			switch (choice) {
			case 1:
				number = Program.bookFirstClass(airline);
				if(number != -1) {
					Program.displayBoardingPass(number, "First Class");
				} else {
					System.out.print("Sorry for inconviniece, but seats are full. Would you like to book Economy? (y/n) : ");
					Scanner s = new Scanner(System.in);
					String status = s.nextLine();
					if(status.equals("y")) {
						number = Program.bookEconomy(airline);
						if(number != -1) {
							Program.displayBoardingPass(number, "Economy");
						}else {
							System.out.println("Next flight leaves in 3 hours");
						}
					}
				}
				break;
			case 2:
				number = Program.bookEconomy(airline);
				if(number != -1) {
					Program.displayBoardingPass(number, "Economy");
				} else {
					System.out.print("Sorry for inconviniece, but seats are full. Would you like to book First Class? (y/n) : ");
					Scanner s = new Scanner(System.in);
					String status = s.nextLine();
					if(status.equals("y")) {
						number = Program.bookFirstClass(airline);
						if(number != -1) {
							Program.displayBoardingPass(number, "First Class");
						}else {
							System.out.println("Next flight leaves in 3 hours");
						}
					}
				}
				break;
			default:
				break;
			}
		}
	}
}
