package test;

public class Rational {
	private int numerator;
	private int denominator;
	
	public Rational() {
		this.numerator = 1;
		this.denominator = 1;
	}
	
	static int gcd(int a, int b) { 
	    if (a == 0) 
	        return b; 
	    return gcd(b % a, a); 
	}
	
	public Rational(int numerator, int denominator) {
		int GCD = Rational.gcd(numerator, denominator);
		this.numerator /= GCD;
		this.denominator /= GCD;
	}
	
	public int getNumerator() {
		return numerator;
	}
	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}
	public int getDenominator() {
		return denominator;
	}
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
	
	public static void addRational(Rational r1, Rational r2) {
		Rational r = new Rational();
		if(r1.denominator != r2.denominator) {
			r.numerator = (r1.numerator * r2.denominator) + (r1.denominator * r2.numerator);
			r.denominator = r1.denominator * r2.denominator;
		}else {
			r.numerator = r1.numerator + r2.numerator;
			r.denominator = r1.denominator;
		}
		int GCD = Rational.gcd(r.numerator, r.denominator);
		r.numerator /= GCD;
		r.denominator /= GCD;
		System.out.println("Addition of " + r1.numerator + "/" + r1.denominator + " and " + r2.numerator + "/" + r2.denominator + " = " + r.numerator + "/" + r.denominator);
	}

	public static void subRational(Rational r1, Rational r2) {
		Rational r = new Rational();
		if(r1.denominator != r2.denominator) {
			r.numerator = (r1.numerator * r2.denominator) - (r1.denominator * r2.numerator);
			r.denominator = r1.denominator * r2.denominator;
		}else {
			r.numerator = r1.numerator - r2.numerator;
			r.denominator = r1.denominator;
		}
		int GCD = Rational.gcd(r.numerator, r.denominator);
		r.numerator /= GCD;
		r.denominator /= GCD;
		System.out.println("Subtraction of " + r1.numerator + "/" + r1.denominator + " and " + r2.numerator + "/" + r2.denominator + " = " + r.numerator + "/" + r.denominator);
	}

	public static void multiplyRational(Rational r1, Rational r2) {
		Rational r = new Rational();
		r.numerator = (r1.numerator * r2.numerator);
		r.denominator = r1.denominator * r2.denominator;
		int GCD = Rational.gcd(r.numerator, r.denominator);
		r.numerator /= GCD;
		r.denominator /= GCD;
		System.out.println("Multiplication of " + r1.numerator + "/" + r1.denominator + " and " + r2.numerator + "/" + r2.denominator + " = " + r.numerator + "/" + r.denominator);
	}

	public static void divideRational(Rational r1, Rational r2) {
		Rational r = new Rational();
		r.numerator = (r1.numerator * r2.denominator);
		r.denominator = r1.denominator * r2.numerator;
		int GCD = Rational.gcd(r.numerator, r.denominator);
		r.numerator /= GCD;
		r.denominator /= GCD;
		System.out.println("Division of " + r1.numerator + "/" + r1.denominator + " and " + r2.numerator + "/" + r2.denominator + " = " + r.numerator + "/" + r.denominator);
	}

	@Override
	public String toString() {
		return "Rational Number = " + this.numerator + "/" + this.denominator;
	}
	
	
}
