package test;

import java.util.Scanner;

public class RationalTest {
	private Scanner sc = new Scanner(System.in);
	
	public int menuList() {
		int choice = 0;
		System.out.println("Operations on Rational Numbers");
		System.out.println("0. Exit");
		System.out.println("1. Addition");
		System.out.println("2. Subtraction");
		System.out.println("3. Multiplication");
		System.out.println("4. Division");
		System.out.println("5. String Representaion");
		System.out.print("Choice	:	");
		choice = sc.nextInt();
		return choice;
	}
	
	public void program() {
		int choice;
		Rational r1 = new Rational();
		Rational r2 = new Rational();
		System.out.print("Numerator of first number : ");
		r1.setNumerator(this.sc.nextInt());
		System.out.print("Denominator of first number : ");
		r1.setDenominator(this.sc.nextInt());
		System.out.print("Numerator of second number : ");
		r2.setNumerator(this.sc.nextInt());
		System.out.print("Denominator of second number : ");
		r2.setDenominator(this.sc.nextInt());

		while((choice = this.menuList()) != 0) {
			switch (choice) {
			case 1:
				Rational.addRational(r1, r2);
				break;
			case 2:
				Rational.subRational(r1, r2);
				break;
			case 3:
				Rational.multiplyRational(r1, r2);
				break;
			case 4:
				Rational.divideRational(r1, r2);
				break;
			case 5:
				r1.toString();
				r2.toString();
				break;
			default:
				break;
			}
		}
	}
}
