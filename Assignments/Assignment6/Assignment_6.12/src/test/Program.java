package test;

import java.util.StringTokenizer;

public class Program {
	public static void main(String[] args) {
		String str = args[0];
		String delims = ".";
		StringTokenizer stk = new StringTokenizer(str, delims, false);
		while (stk.hasMoreTokens()) {
			System.out.println(stk.nextToken());
		}
	}
}
