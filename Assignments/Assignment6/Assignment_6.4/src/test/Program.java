package test;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Program {
	private static String split(String number, String delims) {
		StringTokenizer stk = new StringTokenizer(number, delims, false);
		System.out.println(stk.countTokens());
		String string = "";
		while (stk.hasMoreTokens()) {
			string += stk.nextToken();
		}
		return string;
	}
	public static void main(String[] args) {
		System.out.print("Enter telephone number : ");
		Scanner sc = new Scanner(System.in);
		String number = sc.nextLine();
		String delims = "() -";
		System.out.println("Phone Number : " + Program.split(number, delims));
	}
}
