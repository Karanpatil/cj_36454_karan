package test;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Program {
	public static void main(String[] args) {
		System.out.print("Enter string to be printed : ");
		Scanner sc = new Scanner(System.in);
		String statement = sc.nextLine();
		String delims = " ";
		StringTokenizer stk = new StringTokenizer(statement, delims , false);
		int count = 0;
		while(stk.hasMoreTokens()) {
			String token = stk.nextToken();
			if(token.charAt(0) == 'b') {
				System.out.print("Token starting with 'b' : " + token);
				count++;
			}
		}
		if(count == 0){
			System.out.println("No token in string " + statement + " starting with letter 'b'");
		}
	}
}
