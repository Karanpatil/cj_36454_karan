package test;

import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		System.out.print("Enter string to be printed : ");
		Scanner sc = new Scanner(System.in);
		String statement = sc.nextLine();
		System.out.println("Statement in upper case : " + statement.toUpperCase());
		System.out.println("Statement in lower case : " + statement.toLowerCase());
	}
}
