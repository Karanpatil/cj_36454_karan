package test;

import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		System.out.print("Enter string to be reversed : ");
		Scanner sc = new Scanner(System.in);
		String statement = sc.nextLine();
		
		String[] arrOfStatement = statement.split(" ");
		
		for (int i = arrOfStatement.length - 1; i >= 0; i--) {
			System.out.print(arrOfStatement[i] + " ");
		}
	}
}
