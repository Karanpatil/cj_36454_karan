package test;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Program {
	private static int indexOf(char ch, String str) {
		int count = 0;
		if(str != null) {
			for(int i = 0; i < str.length(); i++) {
				if (str.toLowerCase().charAt(i) == ch) {
					count = i;
					return count;
				}
			}
		}
		return -1;
	}
	private static int lastIndexOf(char ch, String str) {
		int count = 0;
		if(str != null) {
			for(int i = str.length()-1; i >= 0; i--) {
				if (str.charAt(i) == ch) {
					count = i;
					return count;
				}
			}
		}
		return -1;
	}
	public static void main(String[] args) {
		System.out.print("Enter string to be printed : ");
		Scanner sc = new Scanner(System.in);
		String statement = sc.nextLine();
		int index1 = indexOf('d', statement);
		System.out.println("Index of 'd' : " + index1);
		int index2 = lastIndexOf('a', statement);
		System.out.println("Index of 'a' : " + index2);
	}
}
