package test;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Program {
	
	public static void main(String[] args) {
		System.out.print("Enter date in mm/dd/yy format : ");
		Scanner sc = new Scanner(System.in);
		String date = sc.nextLine();
		String delims = "/";
		String[] months = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
		StringTokenizer stk = new StringTokenizer(date, delims, false);
		ArrayList<Integer> arr = new ArrayList<>();
		while (stk.hasMoreTokens()) {
			arr.add(Integer.parseInt(stk.nextToken()));
		}
		
		String newDate = months[arr.get(0) - 1] + " " + arr.get(1) + ", " + arr.get(2);
		System.out.println(newDate);
		
	}
}
