package test;

import java.util.Random;

public class Program {
	
	private static String getRandomArticle(String[] article) {
		int random = new Random().nextInt(article.length);
		return article[random];
	}
	private static String getRandomNoun(String[] noun) {
		int random = new Random().nextInt(noun.length);
		return noun[random];
	}
	private static String getRandomVerb(String[] verb) {
		int random = new Random().nextInt(verb.length);
		return verb[random];
	}
	private static String getRandomPreposition(String[] preposition) {
		int random = new Random().nextInt(preposition.length);
		return preposition[random];
	}
	
	public static void main(String[] args) {
		String[] article = new String[] { "the", "a", "one", "some", "any" };
		String[] noun = new String[] { "boy", "girl", "dog", "town", "car" };
		String[] verb = new String[] { "drove", "jumped", "ran", "walked", "skipped" };
		String[] preposition = new String[] { "to", "from", "over", "under", "on" };
		int number = 0;
		while (number != 20) {
			String art = getRandomArticle(article);
			String sentence = art.substring(0, 1).toUpperCase() + art.substring(1) + " "  +  getRandomNoun(noun) + " " + getRandomVerb(verb) + " " + getRandomPreposition(preposition) + " " + getRandomArticle(article) + " " + getRandomNoun(noun) + ".";
			System.out.println(sentence);
			number++;
		}

	}
}
