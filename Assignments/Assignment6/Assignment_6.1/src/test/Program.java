package test;

import java.util.Scanner;

public class Program {
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.print("Enter First String : ");
		String str1 = sc.nextLine();
		System.out.print("Enter Second String : ");
		String str2 = sc.nextLine();
		
		int var = str1.compareTo(str2);	
		if(var < 0)
			System.out.println(str2 + " is less than " + str1);
		else
			System.out.println(str1 + " is less than " + str2);
	}

}
