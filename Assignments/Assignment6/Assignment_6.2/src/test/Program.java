package test;

import java.util.Scanner;

public class Program {
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.print("First String : ");
		String str1 = sc.nextLine();
		System.out.print("Second String : ");
		String str2 = sc.nextLine();
		System.out.print("Enter number of characters to be compared : ");
		int number = sc.nextInt();
		System.out.print("Enter starting index of comparison string 1 : ");
		int index1 = sc.nextInt();
		System.out.print("Enter starting index of comparison string 2 : ");
		int index2 = sc.nextInt();
		
		if(str1.regionMatches(true, index1-1, str2, index2-1, number-1)) 
			System.out.println(str1 + " and " + str2 + " are equal");
		else
			System.out.println(str1 + " and " + str2 + " are not equal");
	}

}
