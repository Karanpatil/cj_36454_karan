package test;

import java.util.Arrays;

class Address implements Comparable<Address>{
	private String landmark;
	private String city;
	private String state;
	public Address() {
		this("", "", "");
	}
	public Address(String landmark, String city, String state) {
		super();
		this.landmark = landmark;
		this.city = city;
		this.state = state;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (landmark == null) {
			if (other.landmark != null)
				return false;
		} else if (!landmark.equals(other.landmark))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}
	@Override
	public int compareTo(Address other) {
		return this.state.compareTo(other.state);
	}
	@Override
	public String toString() {
		return String.format("%-20s%-12s%-12s", this.landmark, this.city, this.state);
	}
	
}

public class Program {
	private static void print(Address[] addressArray) {
		for (Address address : addressArray)
			System.out.println(address.toString());
		System.out.println();
	}
	public static void main(String[] args) {
		Address[] addressArray = new Address[5];
		addressArray[0] = new Address("Hinjewadi", "Pune", "Maharashtra");
		addressArray[1] = new Address("Global Viilage", "Banglore", "Karnataka");
		addressArray[2] = new Address("Mulgaon", "Panjim", "Goa");
		addressArray[3] = new Address("Ramapuram", "Chennai", "Tamil Nadu");
		addressArray[4] = new Address("Tirumala", "Tirupati", "Andhra Pradesh");
		System.out.println("Before sorting ");
		Program.print(addressArray);
		Arrays.sort(addressArray);
		System.out.println("After sorting ");
		Program.print(addressArray);
	}

}
