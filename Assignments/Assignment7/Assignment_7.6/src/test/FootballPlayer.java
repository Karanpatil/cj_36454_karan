package test;

public class FootballPlayer extends Player {
	private final double matchIncome;
	private final double momIncome ;
	public FootballPlayer() {
		this(0,0);
	}
	public FootballPlayer(double matchIncome, double momIncome) {
		super();
		this.matchIncome = matchIncome;
		this.momIncome = momIncome;
	}
	public double getMatchIncome() {
		return matchIncome;
	}
	public double getMomIncome() {
		return momIncome;
	}
	@Override
	public double calculateIncome() {
		double income;
		income = this.matchIncome + this.momIncome;
		return income;
	}
	@Override
	public String toString() {
		return String.format("%-15f%-15f%-15f", this.matchIncome, this.momIncome, this.income);
	}
}
