package test;

import java.util.Scanner;

public class Program {
	public static void acceptRecord(Player[] player, int count) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Game (Cricket = 0/ Football = 1) : ");
		int game = sc.nextInt() + 1;
		int match, mom;
			switch (game) {
			case 1:
				player[count] = new CricketPlayer(10000,20000);
				System.out.print("Played the match? (0/1) : ");
				match = sc.nextInt();
				System.out.print("Awarded by man of the match? (0/1) : ");
				mom = sc.nextInt();
				player[count].setIncome(player[count].calculateIncome());
				break;
			case 2:
				player[count] = new FootballPlayer(8000,3000);
				System.out.print("Played the match? (0/1) : ");
				match = sc.nextInt();
				System.out.print("Awarded by man of the match? (0/1) : ");
				mom = sc.nextInt();
				player[count].setIncome(player[count].calculateIncome());
				break;
			default:
				break;
			}
	}
	public static void print(Player[] player) {
		if(player != null) {
			System.out.print(String.format("%-15s%-15s%-15s", "Match", "MOM", "Total Income"));
			System.out.println();
			for (Player player2 : player) {
				System.out.println(player2.toString());
			}
		}
	}
	public static void main(String[] args) {
		Player[] player = new Player[10];
		int number = 0;
		while(number < 10) {
			acceptRecord(player, number);
			number++;
		}
		print(player);
	}
}
