package test;

public class Player {
	protected double income;
	
	public double calculateIncome() {
		return 0;
	}
	
	public double getIncome() {
		return income;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	@Override
	public String toString() {
		return String.format("%-10f", this.income);
	}
}
