package test;

public class CricketPlayer extends Player {
	private final double matchIncome ;
	private final double momIncome ;
	public CricketPlayer() {
		this(0,0);
	}
	public CricketPlayer(double matchIncome, double momIncome) {
		super();
		this.matchIncome = matchIncome;
		this.momIncome = momIncome;
	}
	public double getMatchIncome() {
		return matchIncome;
	}
	public double getMomIncome() {
		return momIncome;
	}
	@Override
	public double calculateIncome() {
		double income;
		income = this.matchIncome + this.momIncome;
		return income;
	}
	@Override
	public String toString() {
		return String.format("%-15f%-15f%-15f", this.matchIncome, this.momIncome, this.income);
	}
	
}
