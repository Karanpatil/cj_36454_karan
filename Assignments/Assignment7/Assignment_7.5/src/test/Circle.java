package test;

import java.lang.reflect.Field;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Circle extends Shape{
	private float radius;
	public Circle() {

	}
	public Circle(float radius) {
		this.radius = radius;
	}
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	public float calculateArea(float radius) throws RuntimeException {
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("area");
			field.setAccessible(true);
			float area = (float) (Math.PI * Math.pow(radius, 2));
			field.setFloat(this, area);
			return area;
		} catch (NoSuchFieldException | IllegalAccessException | InputMismatchException e) {
			throw new RuntimeException();
		}
	}
	public void calculateArea() throws RuntimeException {
		Scanner sc = new Scanner(System.in);
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("area");
			field.setAccessible(true);
			
			System.out.print("Radius : ");
			this.radius = sc.nextFloat();
			
			float area = (float) (Math.PI * Math.pow(this.radius, 2));
			field.setFloat(this, area);
		} catch (NoSuchFieldException | IllegalAccessException | InputMismatchException e) {
			throw new RuntimeException();
		}
	}
	public float calculatePerimeter(float radius) throws RuntimeException {
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("perimeter");
			field.setAccessible(true);

			float perimeter = (float) (2 * Math.PI * radius);
			field.setFloat(this, perimeter);
			return perimeter;
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new RuntimeException();
		}
	}
	public void calculatePerimeter() throws RuntimeException {
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("perimeter");
			field.setAccessible(true);

			float perimeter = (float) (2 * Math.PI * this.radius);
			field.setFloat(this, perimeter);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new RuntimeException();
		}
	}
	@Override
	public String toString() {
		return String.format("%-10f%-10f%-10f", this.radius, this.perimeter, this.area);
	}
	
}