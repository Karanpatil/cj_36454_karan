package test;

public class Shape {
	protected float perimeter;
	protected float area;
	public Shape() {

	}
	public void displayArea() {
		System.out.println("Area : " + this.area);
	}
	public void displayPerimeter() {
		System.out.println("Perimeter : " + this.perimeter);
	}
	public void calculateArea() {
		
	}
	public float calculateArea(float radius) {
		return 0.0f;
	}	
	public float calculatePerimeter(float radius) {
		return 0.0f;
	}	
	public void calculatePerimeter() {
		
	}
	
	public float getPerimeter() {
		return perimeter;
	}
	public float getArea() {
		return area;
	}
	@Override
	public String toString() {
		return String.format("%-10f%-10f", this.perimeter, this.area);
	}
	
}