package test;

import java.util.Arrays;
import java.util.Comparator;

class SortByArea implements Comparator<Shape> {
	@Override
	public int compare(Shape o1, Shape o2) {
		if(o1 instanceof Circle && o2 instanceof Circle) {
			Circle c1 = (Circle) o1;
			Circle c2 = (Circle) o2;
			return  (int) (c1.calculateArea(c1.getRadius()) - c2.calculateArea(c2.getRadius()));
		} else if(o1 instanceof Circle && o2 instanceof Rectangle) {
			Circle c1 = (Circle) o1;
			Rectangle c2 = (Rectangle) o2;
			return  (int) (c1.calculateArea(c1.getRadius()) - c2.calculateArea(c2.getHeight(), c2.getWidth()));
		}  else if(o1 instanceof Circle && o2 instanceof Triangle) {
			Circle c1 = (Circle) o1;
			Triangle c2 = (Triangle) o2;
			return  (int) (c1.calculateArea(c1.getRadius()) - c2.calculateArea(c2.getHeight(), c2.getBase()));
		} else if(o1 instanceof Rectangle && o2 instanceof Circle) {
			Rectangle c1 = (Rectangle) o1;
			Circle c2 = (Circle) o2;
			return  (int) (c1.calculateArea(c1.getHeight(), c1.getWidth()) - c2.calculateArea(c2.getRadius()));
		}else if(o1 instanceof Rectangle && o2 instanceof Rectangle) {
			Rectangle c1 = (Rectangle) o1;
			Rectangle c2 = (Rectangle) o2;
			return  (int) (c1.calculateArea(c1.getHeight(), c1.getWidth()) - c2.calculateArea(c2.getHeight(), c2.getWidth()));
		}else if(o1 instanceof Rectangle && o2 instanceof Triangle) {
			Rectangle c1 = (Rectangle) o1;
			Triangle c2 = (Triangle) o2;
			return  (int) (c1.calculateArea(c1.getHeight(), c1.getWidth()) - c2.calculateArea(c2.getHeight(), c2.getBase()));
		}else if(o1 instanceof Triangle && o2 instanceof Circle) {
			Triangle c1 = (Triangle) o1;
			Circle c2 = (Circle) o2;
			return  (int) (c1.calculateArea(c1.getHeight(), c1.getBase()) - c2.calculateArea(c2.getRadius()));
		} else if(o1 instanceof Triangle && o2 instanceof Rectangle) {
			Triangle c1 = (Triangle) o1;
			Rectangle c2 = (Rectangle) o2;
			return  (int) (c1.calculateArea(c1.getHeight(), c1.getBase()) - c2.calculateArea(c2.getHeight(), c2.getWidth()));
		} else {
			Triangle c1 = (Triangle) o1;
			Triangle c2 = (Triangle) o2;
			return  (int) (c1.calculateArea(c1.getHeight(), c1.getBase()) - c2.calculateArea(c2.getHeight(), c2.getBase()));
		}
	}
}
class SortByPerimeter implements Comparator<Shape> {
	@Override
	public int compare(Shape o1, Shape o2) {
		if(o1 instanceof Circle && o2 instanceof Circle) {
			Circle c1 = (Circle) o1;
			Circle c2 = (Circle) o2;
			return  (int) (c1.calculatePerimeter(c1.getRadius()) - c2.calculatePerimeter(c2.getRadius()));
		} else if(o1 instanceof Circle && o2 instanceof Rectangle) {
			Circle c1 = (Circle) o1;
			Rectangle c2 = (Rectangle) o2;
			return  (int) (c1.calculatePerimeter(c1.getRadius()) - c2.calculatePerimeter(c2.getHeight(), c2.getWidth()));
		}  else if(o1 instanceof Circle && o2 instanceof Triangle) {
			Circle c1 = (Circle) o1;
			Triangle c2 = (Triangle) o2;
			return  (int) (c1.calculatePerimeter(c1.getRadius()) - c2.calculatePerimeter(c2.getSide1(), c2.getSide2(), c2.getBase()));
		} else if(o1 instanceof Rectangle && o2 instanceof Circle) {
			Rectangle c1 = (Rectangle) o1;
			Circle c2 = (Circle) o2;
			return  (int) (c1.calculatePerimeter(c1.getHeight(), c1.getWidth()) - c2.calculatePerimeter(c2.getRadius()));
		}else if(o1 instanceof Rectangle && o2 instanceof Rectangle) {
			Rectangle c1 = (Rectangle) o1;
			Rectangle c2 = (Rectangle) o2;
			return  (int) (c1.calculatePerimeter(c1.getHeight(), c1.getWidth()) - c2.calculatePerimeter(c2.getHeight(), c2.getWidth()));
		}else if(o1 instanceof Rectangle && o2 instanceof Triangle) {
			Rectangle c1 = (Rectangle) o1;
			Triangle c2 = (Triangle) o2;
			return  (int) (c1.calculatePerimeter(c1.getHeight(), c1.getWidth()) - c2.calculatePerimeter(c2.getSide1(), c2.getSide2(), c2.getBase()));
		}else if(o1 instanceof Triangle && o2 instanceof Circle) {
			Triangle c1 = (Triangle) o1;
			Circle c2 = (Circle) o2;
			return  (int) (c1.calculatePerimeter(c1.getSide1(), c1.getSide2(), c1.getBase()) - c2.calculatePerimeter(c2.getRadius()));
		} else if(o1 instanceof Triangle && o2 instanceof Rectangle) {
			Triangle c1 = (Triangle) o1;
			Rectangle c2 = (Rectangle) o2;
			return  (int) (c1.calculatePerimeter(c1.getSide1(), c1.getSide2(), c1.getBase()) - c2.calculatePerimeter(c2.getHeight(), c2.getWidth()));
		} else {
			Triangle c1 = (Triangle) o1;
			Triangle c2 = (Triangle) o2;
			return  (int) (c1.calculatePerimeter(c1.getSide1(), c1.getSide2(), c1.getBase()) - c2.calculatePerimeter(c2.getSide1(), c2.getSide2(), c2.getBase()));
		}
	}
}
public class Program {

	private static void print(Shape[] arr) {
		if( arr != null ) {
			for (Shape shape : arr) 	
				System.out.println(shape.toString());
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Shape[] arr = new Shape[5];
		arr[0] = new Circle(5.5f);
		arr[1] = new Rectangle(10.5f, 25.5f);
		arr[2] = new Circle(47.5f);
		arr[3] = new Triangle(7.4f, 9.8f, 4.1f, 6.9f);
		arr[4] = new Circle(18.9f);
		Program.print(arr);
		Arrays.sort(arr, new SortByArea());
		Program.print(arr);
		Arrays.sort(arr, new SortByPerimeter());
		Program.print(arr);
	}
}