package test;

import java.lang.reflect.Field;
import java.util.Scanner;

public class Triangle extends Shape{
	private float base;
	private float height;
	private float side1;
	private float side2;	
	public Triangle() {
		this(0.0f, 0.0f, 0.0f, 0.0f);
	}
	public Triangle(float base, float height, float side1, float side2) {
		this.base = base;
		this.height = height;
		this.side1 = side1;
		this.side2 = side2;
	}
	public float getBase() {
		return base;
	}
	public void setBase(float base) {
		this.base = base;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getSide1() {
		return side1;
	}
	public void setSide1(float side1) {
		this.side1 = side1;
	}
	public float getSide2() {
		return side2;
	}
	public void setSide2(float side2) {
		this.side2 = side2;
	}
	public float calculateArea(float height, float base) throws RuntimeException {
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("area");
			field.setAccessible(true);
			float area = base * height / 2;
			field.setFloat(this, area);
			return area;
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new RuntimeException();
		}
	}
	public void calculateArea() throws RuntimeException {
		Scanner sc = new Scanner(System.in);
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("area");
			field.setAccessible(true);

			System.out.print("Height : ");
			this.height = sc.nextFloat();
			System.out.print("Base : ");
			this.base = sc.nextFloat();
			
			float area = this.base * this.height / 2;
			field.setFloat(this, area);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new RuntimeException();
		}
	}
	public float calculatePerimeter(float side1, float side2, float side3) throws RuntimeException {
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("perimeter");
			field.setAccessible(true);

			float perimeter = this.side1 + this.side2 + this.base;
			field.setFloat(this, perimeter);
			return perimeter;
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new RuntimeException();
		}
	}
	public void calculatePerimeter() throws RuntimeException {
		Scanner sc = new Scanner(System.in);
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("perimeter");
			field.setAccessible(true);

			System.out.print("Side 1 : ");
			this.side1 = sc.nextFloat();
			System.out.print("Side 2 : ");
			this.side2 = sc.nextFloat();
			System.out.print("Side 3 : ");
			this.base = sc.nextFloat();

			float perimeter = this.side1 + this.side2 + this.base;
			field.setFloat(this, perimeter);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new RuntimeException();
		}
	}
	@Override
	public String toString() {
		return String.format("%-10f%-10f%-10f%-10f", this.height, this.base, this.perimeter, this.area);
	}
	
}