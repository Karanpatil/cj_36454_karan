package test;

import java.lang.reflect.Field;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Rectangle extends Shape {
	private float width;
	private float height;
	public Rectangle() {
		this(0.0f, 0.0f);
	}
	public Rectangle(float width, float height) {
		super();
		this.width = width;
		this.height = height;
	}
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float calculateArea(float height, float width) throws RuntimeException {
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("area");
			field.setAccessible(true);

			float area = height * width;
			field.setFloat(this, area);
			return area;
		} catch (NoSuchFieldException | IllegalAccessException | InputMismatchException e) {
			throw new RuntimeException();
		}
	}
	public void calculateArea() throws RuntimeException {
		Scanner sc = new Scanner(System.in);
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("area");
			field.setAccessible(true);

			System.out.print("Height : ");
			this.height = sc.nextFloat();
			System.out.print("Width : ");
			this.width = sc.nextFloat();
			
			float area = this.height * this.width;
			field.setFloat(this, area);
		} catch (NoSuchFieldException | IllegalAccessException | InputMismatchException e) {
			throw new RuntimeException();
		}
	}
	public float calculatePerimeter(float height, float width) throws RuntimeException {
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("perimeter");
			field.setAccessible(true);

			float perimeter = 2 * (height + width);
			field.setFloat(this, perimeter);
			return perimeter;
		} catch (NoSuchFieldException | IllegalAccessException | InputMismatchException e) {
			throw new RuntimeException();
		}
	}
	public void calculatePerimeter() throws RuntimeException {
		try {
			Class<?> c = this.getClass().getSuperclass();
			Field field = c.getDeclaredField("perimeter");
			field.setAccessible(true);

			float perimeter = 2 * (this.height + this.width);
			field.setFloat(this, perimeter);
		} catch (NoSuchFieldException | IllegalAccessException | InputMismatchException e) {
			throw new RuntimeException();
		}
	}
	@Override
	public String toString() {
		return String.format("%-10f%-10f%-10f%-10f", this.height, this.width, this.perimeter, this.area);
	}
	
}