package test;

import java.util.Arrays;
class Date implements Comparable<Date>{
	private int day;
	private int month;
	private int year;
	public Date() {
		this(0,0,0);
	}
	public Date(int year, int month, int day) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	@Override
	public int compareTo(Date other) {
		return this.year - other.year;
	}
	@Override
	public String toString() {
		return String.format("%-2d/ %-2d/ %-1d", this.day,this.month,this.year);
	}
}
class Person implements Comparable<Person>{
	private String name;
	private String address;
	private Date birthDate;
	public Person(String name, String address, Date birthDate) {
		super();
		this.name = name;
		this.address = address;
		this.birthDate = birthDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getBirthDate() {
		return this.birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		Person other = (Person) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public int compareTo(Person other) {
		return this.birthDate.getYear() - other.birthDate.getYear();
	}
	@Override
	public String toString() {
		return String.format("%-20s%-12s%-12s", this.name, this.address, this.birthDate.toString());
	}
	
}

public class Program {
	private static void print(Person[] personArray) {
		for (Person person : personArray)
			System.out.println(person.toString());
		System.out.println();
	}
	public static void main(String[] args) {
		Person[] personArray = new Person[5];
		personArray[0] = new Person("Tejas", "Pune", new Date(1998,1,25));
		personArray[1] = new Person("Dnyanesh", "Kolhapur", new Date(1996,8,12));
		personArray[2] = new Person("Shardool", "Melbourne", new Date(1991,12,30));
		personArray[3] = new Person("Shubham", "Mumbai", new Date(1994,5,9));
		personArray[4] = new Person("Mandar", "Satara", new Date(2005,7,7));
		Program.print(personArray);
		Arrays.sort(personArray);
		System.out.println("After sorting ");
		Program.print(personArray);
	}

}
