package test;

import java.util.Arrays;

class Date implements Comparable<Date>{
	private int day;
	private int month;
	private int year;
	public Date() {
		this(0,0,0);
	}
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	@Override
	public int compareTo(Date other) {
		return this.year - other.year;
	}
	@Override
	public String toString() {
		return String.format("%-2d/ %-2d/ %-1d", this.day,this.month,this.year);
	}
}

public class Program {
	private static void print(Date[] dateArray) {
		for (Date date : dateArray)
			System.out.println(date.toString());
		System.out.println();
	}
	public static void main(String[] args) {
		Date[] dateArray = new Date[5];
		dateArray[0] = new Date(1, 1, 2015);
		dateArray[1] = new Date(2, 5, 2010);
		dateArray[2] = new Date(3, 4, 2020);
		dateArray[3] = new Date(4, 9, 2005);
		dateArray[4] = new Date(5, 12, 2017);
		System.out.println("Before sorting ");
		Program.print(dateArray);
		Arrays.sort(dateArray);
		System.out.println("After sorting ");
		Program.print(dateArray);
	}

}
