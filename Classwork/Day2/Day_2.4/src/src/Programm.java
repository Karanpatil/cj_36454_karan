package src;

public class Programm {

	public static void main(String[] args) {
		String str = "125";
		int number = Integer.parseInt(str);
		System.out.println("Number : " + number);
	}

	public static void main3(String[] args) {
		double number = 10.5;
		String str = Double.toString(number);
		System.out.println("Number : " + str);
	}

	public static void main2(String[] args) {
		char ch = 'A';
		String str = Character.toString(ch);
		System.out.println("Number : " + str);
	}

	public static void main1(String[] args) {
		int number = 10;
		String strNumber = Integer.toString(number);
		System.out.println("Number : " + strNumber);
	}

}
