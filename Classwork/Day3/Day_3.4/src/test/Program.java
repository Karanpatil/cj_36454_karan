package test;

class Account {
	private int number;
	private String type;
	private float balance;
	
	public Account() {
		
	}
	
	public Account(int number, String type, float balance) {
		this.number = number;
		this.type = type;
		this.balance = balance;
	}

	public void printRecord() {
		System.out.println("Numer	:	" + this.number);
		System.out.println("Type	:	" + this.type);
		System.out.println("Balance	:	" + this.balance);
	}
}

public class Program {

	public static void main(String[] args) {
		Account account = new Account(100, "Saving", 500000.0f);
		account.printRecord();
	}
	
	public static void main2(String[] args) {
		Account account = null;
		account.printRecord();	//Compiler error
	}
	
	public static void main1(String[] args) {
		//Account account;
		//account.printRecord();	//Compiler error
	}

}