package test;

import java.util.Scanner;

class Account {
	private int number;
	private String type;
	private float balance;
	
	public void acceptRecord() {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Number	:	");
		this.number = sc.nextInt();
		
		System.out.print("Type	:	");
		sc.nextLine();
		this.type = sc.nextLine();
		
		System.out.print("Balance	:	");
		this.balance = sc.nextFloat();
		
		sc.close();
	}

	public void printRecord() {
		System.out.println("Numer : " + this.number);
		System.out.println("Type : " + this.type);
		System.out.println("Balance : " + this.balance);
	}
}

public class Program {

	public static void main(String[] args) {
		Account account = new Account();
		account.acceptRecord();
		account.printRecord();
	}

	public static void main1(String[] args) {
		//new Account();	//Anonymous instance
		
		Account account = new Account();
	}

}
