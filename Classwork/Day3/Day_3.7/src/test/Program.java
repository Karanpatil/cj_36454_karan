package test;

public class Program {

	public static void main(String[] args) {
		int choice;
		AccountTest test = new AccountTest();
		Account account = new Account();
		while ((choice = AccountTest.menuList()) != 0) {
			switch (choice) {
			case 1:
				test.acceptRecord(account);
				break;
			case 2:
				test.printRecord(account);
				break;
			default:
				break;
			}
		}
	}
}