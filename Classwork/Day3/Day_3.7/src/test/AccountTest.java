package test;

import java.util.Scanner;

public class AccountTest {
	private Account account = new Account();
	static Scanner sc = new Scanner(System.in);

	public void acceptRecord(Account account) {

		System.out.print("Number : ");
		this.account.setNumber(sc.nextInt());

		System.out.print("Type : ");
		sc.nextLine();
		this.account.setType(sc.nextLine());

		System.out.print("Balance : ");
		this.account.setBalance(sc.nextFloat());

	}

	public void printRecord(Account account) {
		System.out.println("Number	:	" + this.account.getNumber());
		System.out.println("type	:	" + this.account.getType());
		System.out.println("Balance	:	" + this.account.getBalance());
	}

	public static int menuList() {
		System.out.println("0. Exit");
		System.out.println("1. Accept Record");
		System.out.println("2. Print Record");
		System.out.println("Enter choice	:	");
		int choice = sc.nextInt();
		return choice;

	}

}
