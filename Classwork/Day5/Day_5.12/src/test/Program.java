package test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	
	static Scanner sc = new Scanner(System.in);
	private static void acceptArray(int[] arr) {
		if( arr != null ) {
			for( int index = 0; index < arr.length; ++ index ) {
				System.out.print("arr[ "+index+" ]	:	");
				arr[ index ] = sc.nextInt();
			}
		}
	}
	
	private static void printArray(int[] arr) {
		if(arr != null) {
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
		}
	}

	public static void main(String[] args) {
		int[] arr = new int[] { 10, 20, 30, 40, 50 };
		System.out.println(Arrays.toString(arr));
	}

	public static void main11(String[] args) {
		int[] arr = new int[] { 10, 20, 30, 40, 50 };
		for( int element: arr )		//forward only and read only
			System.out.println(element);
	}

	public static void main10(String[] args) {
		int[] arr = new int[] {50, 10, 40, 30, 20};
		Program.printArray(arr);
		Arrays.sort(arr);
		Program.printArray(arr);
	}
	
	public static void main9(String[] args) {
		int[] arr = new int[] {10,20,30};
		//int value = arr[arr.length];	//ArrayIndexOutOfBoundException
		Program.printArray(arr);
	}

	public static void main8(String[] args) {
		int[] arr1 = new int[] {10,20,30};
		int[] arr2 = new int[3];
		Program.printArray(arr1);
	}

	public static void main7(String[] args) {
		int[] arr = new int[ 3 ];
		Program.acceptArray(arr);
		Program.printArray(arr);
	}

	public static void main6(String[] args) {
		Object[] arr = new String[3];
		arr[0] = new String("DAC");
		arr[1] = new Integer(10);	//Exception
	}

	public static void main5(String[] args) {
		int[] arr = new int[3];
		Program.printArray(arr);
		
		Program.printArray(null);
	}


	public static void main4(String[] args) {
		int[] arr = new int[3];
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
	}
	
	public static void main3(String[] args) {
		int[] arr = new int[-3];	//NegativeArraySizeException
	}
	
	public static void main2(String[] args) {
		int arr[]  = null;
		arr = new int[3];
		
		//int[] arr = new int[3];  //OK
		//int arr[] = new int[3];  //OK
		
	}

	public static void main1(String[] args) {
		//new int[3];
		//int arr[];  //OK
		//int[] arr;  //OK
		//int [arr];  //OK
	}
}
