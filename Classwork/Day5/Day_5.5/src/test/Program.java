package test;

class Test{
	public static int square(int number) {
		return number * number;
	}
}

public class Program {
	public static void main(String[] args) {
		int number = Test.square(5);
		System.out.println("Number : " + number);
	}

}
