package test;

class Test{
	public static final int num1 = 10;
	public static final int num2 = 20;
	public Test() {
		//this.num1 = 10;
	}
	
	public void print() {
		System.out.println("Num1 : " + Test.num1);
		System.out.println("Num2 : " + Test.num2);
	}
}

public class Program {
	public static void main(String[] args) {
		Test t = new Test();
		t.print();

	}
}
