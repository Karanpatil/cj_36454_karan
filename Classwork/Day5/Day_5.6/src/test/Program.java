package test;

class Test{
	private static int count;
	public static void print() {
		count += 1;
		System.out.println("Count : " + count);
	}
}

public class Program {
	public static void main(String[] args) {
		Test.print();
		Test.print();
		Test.print();
	}

}
