package test;

class Test{
	static {
		System.out.println("Static Block 1");
		Test.num1 = 10;
	}
	private static int num1;
	private static int num2;
	private static int num3;
	
	/*
	 * static { Test.num1 = 10; Test.num2 = 20; Test.num3 = 30; }
	 */	

	static {
		System.out.println("Static Block 2");
		Test.num2 = 20;
	}

	public static void print() {
		System.out.println("Num1 : " + Test.num1);
		System.out.println("Num2 : " + Test.num2);
		System.out.println("Num3 : " + Test.num3);
	}
	
	static {
		System.out.println("Static Block 3");
		Test.num3 = 30;
	}

}

public class Program {

	public static void main(String[] args) {
		System.out.println("Inside main method");
		Test.print();
	}

}
