package test;

public class Program {
	public static void main(String[] args) {
		Object a = new Object() {
			private String message = "Inside anonymous inner class";
			
			@Override
			public String toString() {
				return this.message;
			}
		};
		
		System.out.println(a.toString());
	}
}
