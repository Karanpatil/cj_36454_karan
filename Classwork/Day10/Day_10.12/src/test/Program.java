package test;

interface A {
	public static final int number = 0;
	
	void print();
}
class B implements A{

	@Override
	public void print() {
		System.out.println("Number : " + A.number);
	}
	
}
public class Program {
	public static void main(String[] args) {
		A a = new B();
		a.print();
	}

	public static void main1(String[] args) {
		B b = new B();
		b.print();
	}
}
