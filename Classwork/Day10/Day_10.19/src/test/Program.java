package test;

import java.util.Arrays;
import java.util.Comparator;

class Employee{
	private String name;
	private int empid;
	private float salary;
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	public String getName() {
		return name;
	}
	public int getEmpid() {
		return empid;
	}
	public float getSalary() {
		return salary;
	}
	@Override
	public String toString() {
		return String.format("%-15s%-5d%-10.2f", this.name, this.empid, this.salary);
	}
}
class SortByName implements Comparator<Employee>{
	@Override
	public int compare(Employee emp1, Employee emp2) {
		return emp1.getName().compareTo(emp2.getName());
	}
}
class SortByEmpid implements Comparator<Employee>{
	@Override
	public int compare(Employee emp1, Employee emp2) {
		return emp1.getEmpid() - emp2.getEmpid();
	}
}
class SortBySalary implements Comparator<Employee>{
	@Override
	public int compare(Employee emp1, Employee emp2) {
		return (int) (emp1.getSalary() - emp2.getSalary());
	}
}

public class Program {	
	public static Employee[] getEmployees( ) {
		Employee[] arr = new Employee[ 5 ];
		arr[ 0 ] = new Employee("Prashant", 13, 50000);
		arr[ 1 ] = new Employee("Amol", 11, 40000);
		arr[ 2 ] = new Employee("Rupesh", 15, 30000);
		arr[ 3 ] = new Employee("Umesh", 14, 20000);
		arr[ 4 ] = new Employee("Mukesh", 12, 10000);
		return arr;
	}
	private static void print(Employee[] arr, Comparator<Employee> c) {
		if( arr != null ) {
			Arrays.sort( arr, c );
			for( Employee emp : arr )
				System.out.println(emp.toString());
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Employee[] arr = Program.getEmployees();
		System.out.println();
		Program.print( arr, new SortByName() );
		Program.print( arr, new SortByEmpid() );
		Program.print( arr, new SortBySalary() );
	}
}