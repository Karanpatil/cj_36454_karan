package test;

public class Program {
	public static void main(String[] args) {
		class Complex{
			private int real;
			private int imag;
			public Complex(int real, int imag) {
				super();
				this.real = real;
				this.imag = imag;
			}
			public int getReal() {
				return real;
			}
			public void setReal(int real) {
				this.real = real;
			}
			public int getImag() {
				return imag;
			}
			public void setImag(int imag) {
				this.imag = imag;
			}
			
		}
	}
}
