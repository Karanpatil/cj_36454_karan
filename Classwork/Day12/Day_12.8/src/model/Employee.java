package model;

import annotations.Column;
import annotations.Id;
import annotations.Table;

@Table( name = "employees")
public class Employee {
	@Column( name = "emp_name", columnDefinition = "VARCHAR(100)")
	private String name;
	@Id
	@Column( name = "emp_id", columnDefinition = "INT")
	private int empid;
	@Column( name = "sal", columnDefinition = "FLOAT")
	private float salary;

	public Employee() {
	}

	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", empid=" + empid + ", salary=" + salary + "]";
	}
}
