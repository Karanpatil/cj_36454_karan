package test;

import java.util.ArrayList;
import java.util.Collections;

public class ListTest {
	ArrayList<Integer> list = new ArrayList<>();
	public void add( Integer element ) {
		if( this.list != null ) {
			this.list.add(element);
		}
	}
	public Integer find( Integer element ) {
		if( this.list != null) {
			Integer key = new Integer(element);
			if( list.contains(key))
			{
				int index = this.list.indexOf(key);
				return index;
			}
		}
		return null;
	}
	/*public boolean remove( Integer element ) {
		Integer index = this.find(element);
		if( index != null ) {
			this.list.remove(index);	//Method of List
			return true;
		}
		return false;
	}*/
	public boolean remove( Integer element ) {
		if( this.list != null) {
			Integer key = new Integer(element);
			if( list.contains(key))
			{
				this.list.remove(key);	//Method Of Collection
				return true;
			}
		}
		return false;
	}
	public void print( ) {
		if( this.list != null ) {
			//Collections.sort(this.list);
			this.list.sort(null);
			for (Integer element : list) 
				System.out.println(element+"	");
			System.out.println();
		}else {
			System.out.println("List is empty");
		}
	}
}