package test;
public class Program {
	public static int getHashCode( int element ) {
		int result = 1;
		final int prime = 31;
		result = result * element + prime * element;
		return result;
	}
	public static void main(String[] args) {
		for (int count = 0; count <= 100; count++) {
			int a = count;
			int hashCode = Program.getHashCode(a);
			int slot = hashCode % 5;
			System.out.println(a + "   " + hashCode + "   " + slot);
		}
	}

	public static void main3(String[] args) {
		int a = 10;
		int hashCode = Program.getHashCode(a);
		int slot = hashCode % 5;
		System.out.println(a + "   " + hashCode + "   " + slot);
	}
	public static void main2(String[] args) {
		int a = 10;
		System.out.println(Program.getHashCode(a));

		int b = 15;
		System.out.println(Program.getHashCode(b));
		
	}
	public static void main1(String[] args) {
		int a = 10;
		System.out.println(Program.getHashCode(a));

		int b = 10;
		System.out.println(Program.getHashCode(b));
	}
}