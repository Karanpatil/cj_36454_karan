package test;

import java.util.ArrayList;

public class Program {
	
	private static ArrayList<Integer> getIntegerList(){
		ArrayList<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(20);
		list.add(30);
		return list;
	}
	
	private static ArrayList<Double> getDoubleList(){
		ArrayList<Double> list = new ArrayList<>();
		list.add(10.1);
		list.add(20.2);
		list.add(30.3);
		return list;
	}
	private static ArrayList<String> getStringList(){
		ArrayList<String> list = new ArrayList<>();
		list.add("DAC");
		list.add("DMC");
		list.add("DESD");
		return list;
	}
	
	private static void showList(ArrayList<Integer>list) {
		if(list != null) {
			for (Integer element : list) {
				System.out.println(element);
			}
		}
	}
	private static void printList(ArrayList<Double>list) {
		if(list != null) {
			for (Double element : list) {
				System.out.println(element);
			}
		}
	}
	private static void displayList(ArrayList<String>list) {
		if(list != null) {
			for (String element : list) {
				System.out.println(element);
			}
		}
	}
	public static void main(String[] args) {
		ArrayList<Integer>intList = Program.getIntegerList();
		Program.showList(intList);

		ArrayList<Double>doubleList = Program.getDoubleList();
		Program.printList(doubleList);

		ArrayList<String>stringList = Program.getStringList();
		Program.displayList(stringList);
}
	
	public static void main1(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(20);
		list.add(30);
		
		for (Integer element : list) {
			System.out.println(element);
		}
	}
}
