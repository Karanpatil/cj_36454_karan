package test;

import java.util.Arrays;

public class Program {
	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder("Sunbeam");
		String str = new String(sb);
		System.out.println(str);
	}
	public static void main7(String[] args) {
		StringBuffer sb = new StringBuffer("Sunbeam");
		String str = new String(sb);
		System.out.println(str);
	}
	public static void main6(String[] args) {
		String str = new String("Sunbeam");
		String str2 = new String(str);
		System.out.println(str2);
	}
	public static void main5(String[] args) {
		char[] data = new char[] {'D', 'A', 'C'};
		String str = new String(data);
		System.out.println(str);
	}
	public static void main4(String[] args) {
		byte[] bs = new byte[] {65, 66, 67};
		String str = new String(bs);
		System.out.println(str);
	}
	public static void main3(String[] args) {
		String str = "ABC";
		byte[] bs = str.getBytes();
		System.out.println(Arrays.toString(bs));
	}
	public static void main2(String[] args) {
		String s1 = new String();
		System.out.println(s1.isEmpty() + " " + s1.length());
	}
	
	public static void main1(String[] args) {
		String str = "Pune";
		System.out.println(str.charAt(0));
		System.out.println(str.charAt(1));
		System.out.println(str.charAt(2));
		System.out.println(str.charAt(3));
		System.out.println(str.charAt(4));	//Exception - No null character 
		
	}
}
