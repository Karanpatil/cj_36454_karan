package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojo.Book;
import utils.DBUtils;

public class BookDao implements Closeable{
	Connection connection = null;
	CallableStatement stmtInsert;
	CallableStatement stmtUpdate;
	CallableStatement stmtDelete;
	CallableStatement stmtSelect;

	public BookDao() throws Exception{
		 this.connection = DBUtils.getConnection();
		 this.stmtInsert = this.connection.prepareCall("{call sp_insert_book(?,?,?,?,?)}");
		 this.stmtUpdate = this.connection.prepareCall("{call sp_update_book(?,?)}");
		 this.stmtDelete = this.connection.prepareCall("{call sp_delete_book(?)}");
		 this.stmtSelect = this.connection.prepareCall("{call sp_select_book()}");
	}
	public int insert(Book book) throws Exception{
		this.stmtInsert.setInt(1, book.getBookId());
		this.stmtInsert.setString(2, book.getSubjectName());
		this.stmtInsert.setString(3, book.getBookName());
		this.stmtInsert.setString(4, book.getAuthorName());
		this.stmtInsert.setFloat(5, book.getPrice());
		this.stmtInsert.execute();
		return stmtInsert.getUpdateCount();
	}
	public int update(int bookId, float price) throws Exception{
		this.stmtUpdate.setFloat(1, price);
		this.stmtUpdate.setInt(2, bookId);
		this.stmtUpdate.execute();
		return stmtUpdate.getUpdateCount();
	}
	public int delete(int bookId) throws Exception{
		this.stmtDelete.setInt(1, bookId);
		this.stmtDelete.execute();
		return stmtDelete.getUpdateCount();
	}
	public List<Book> getBooks( )throws Exception{
		List<Book> books = new ArrayList<Book>();
		this.stmtSelect.execute();
		try( ResultSet rs = this.stmtSelect.getResultSet() ){
			while( rs.next()) {
				Book book = new Book(rs.getInt("book_id"),rs.getString("subject_name"),rs.getString("book_name"),rs.getString("author_name"),rs.getFloat("price"));
				books.add( book );
			}
		}
		return books;
	}
	@Override
	public void close() throws IOException {
		try {
			connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause);
		}
	}
}