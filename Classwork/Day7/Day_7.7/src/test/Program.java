package test;

public class Program {
	
	public static void showRecord() {
		try {
			for (int i = 0; i <= 10; i++) {
				System.out.println("count : " + i);
				Thread.sleep(200);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void displayRecord() throws InterruptedException {
		for (int i = 0; i <= 10; i++) {
			System.out.println("count : " + i);
			Thread.sleep(300);
		}
	}

	public static void main(String[] args) {
		//Program.showRecord();
		try {
			Program.displayRecord();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
