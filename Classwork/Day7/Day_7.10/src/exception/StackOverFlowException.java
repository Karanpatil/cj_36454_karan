package exception;

@SuppressWarnings("serial")
public class StackOverFlowException extends Exception {

	public StackOverFlowException(String message) {
		super(message);
	}
	
}
