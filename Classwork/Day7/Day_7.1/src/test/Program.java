package test;
//Resource Type => Test class;
class Test implements AutoCloseable{

	public Test() {
		System.out.println("Inside constructor");
	}
	
	public void print() {
		System.out.println("Inside print");
	}
	
	@Override
	public void close() throws Exception {
		System.out.println("Inside close");
	}
}

public class Program {

	public static void main(String[] args) {
		Test t = null;
		t = new Test( );	//Resource
	}
}
