package test;

class Rectangle{
	private float area;
	private float length;
	private float breadth;
	
	public Rectangle() {

	}
	
	public void setLength(float length) {
		this.length = length;
	}
	
	public void setBreadth(float breadth) {
		this.breadth = breadth;
	}
	
	public void calculateArea() {
		this.area = this.length * this.breadth;
	}
	
	public float getArea() {
		return area;
	}
}

class Circle{
	private float area;
	private float radius;
	
	public Circle() {

	}
	
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public void calculateArea() {
		this.area = (float) (Math.PI * Math.pow(this.radius, 2));
	}
	
	public float getArea() {
		return area;
	}
}

public class Program {
	
	public static void main(String[] args) {
		Circle c = new Circle();
		c.setRadius(10);
		c.calculateArea();
		System.out.println(c.getArea());
	}
	
	public static void main1(String[] args) {
		Rectangle r = new Rectangle();
		r.setLength(10);
		r.setBreadth(20);
		r.calculateArea();
		r.getArea();
	}
}
