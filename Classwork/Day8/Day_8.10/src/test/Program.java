package test;

import java.util.Date;

class Box<T>{
	private T object;
	public T getObject() {
		return object;
	}
	public void setObject(T object) {
		this.object = object;
	}
}

public class Program {
	public static void main(String[] args) {
		//Box<int> box = new Box<>( );	//Not OK
		Box<Integer> box = new Box<>( );
		box.setObject(10);
		Integer number = box.getObject();
	}
	public static void main2(String[] args) {
		Box box = new Box();	//Raw Type : Box
		//Box<Object> box = new Box<>();
	}
	public static void main1(String[] args) {
		Box<Date> b4 = new Box<Date>();
		b4.setObject(new Date());
		Date date =  b4.getObject();
		System.out.println(date);
	}
}
