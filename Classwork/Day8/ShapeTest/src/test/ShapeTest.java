package test;

import java.util.Scanner;

import org.sunbeam.dac.lib.*;

public class ShapeTest{
	private Shape shape = null;
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	static Scanner sc = new Scanner(System.in);
	public void acceptRecord( ) {
		if( shape instanceof Rectangle) {
			Rectangle rect = (Rectangle) shape; //Downcasting
			System.out.print("Length	:	");
			rect.setLength(sc.nextFloat());
			System.out.print("Breadth	:	");
			rect.setBreadth(sc.nextFloat());
			rect.calculateArea();
		}else {
			Circle c = (Circle) shape; //Downcasting
			System.out.print("Radius	:	");
			c.setRadius(sc.nextFloat());
			c.calculateArea();
		}
	}
	public void printRecord( ) {
		String className = shape.getClass().getSimpleName();
		System.out.println("Area of instance of class "+className+" is : "+shape.getArea());
	}
	public static int menuList() {
		System.out.println("0.Exit");
		System.out.println("1.Rectangle");
		System.out.println("2.Circle");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
}