package test;

import java.util.Scanner;

import org.sunbeam.dac.lib.ShapeFactory;

public class Program {
	public static void main(String[] args) {
		int choice;
		ShapeTest test = new ShapeTest();
		while ((choice = ShapeTest.menuList()) != 0) {
			test.setShape(ShapeFactory.getInstance(choice));
			test.acceptRecord();
			test.printRecord();
		}
	}
}