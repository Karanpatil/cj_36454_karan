package test;

import java.util.Date;

class Employee {
	private String name;
	private int empid;
	private String department;
	private String designation;
	private float salary;
	
	public Employee() {
		this("", 0, "", "", 0);
	}
	
	public Employee(String name, int empid, String department, String designation, float salary) {
		super();
		this.name = name;
		this.empid = empid;
		this.department = department;
		this.designation = designation;
		this.salary = salary;
	}
	
	/*
	 * public String getString() { String str = null;
	 * String.format("%-15s%-5d%-10s%-10s%-10.2f\n", this.name, this.empid,
	 * this.department, this.designation, this.salary); return str; }
	 */	
	
	public String getString() {
		return String.format("%-15s%-5d%-10s%-10s%-10.2f\n", this.name, this.empid, this.department, this.designation, this.salary);
	}
	
	/*
	 * @Override public String toString() { return
	 * String.format("%-15s%-5d%-10s%-10s%-10.2f\n", this.name, this.empid,
	 * this.department, this.designation, this.salary); }
	 */	
		
	  public void printRecord( ) {
		  System.out.printf("%-15s%-5d%-10s%-10s%-10.2f\n", this.name, this.empid,
		  this.department, this.designation, this.salary); 
	  }

	@Override
	public String toString() {
		return "Employee [name=" + name + ", empid=" + empid + ", department=" + department + ", designation="
				+ designation + ", salary=" + salary + "]";
	}	
	
}

public class Program {
	
	public static void main(String[] args) {
		Employee emp = new Employee("Dnyanesh", 20, "abc", "aa", 12345.67f);
		String str = emp.toString();
		System.out.println(str);
	}

	public static void main4(String[] args) {
		Date date = new Date();
		String str = date.toString();
		System.out.println(str);
	}
	
	public static void main3(String[] args) {
		Integer number = new Integer(125);
		String str = number.toString();
		System.out.println(str);
	}

	public static void main2(String[] args) {
		Employee emp = new Employee("Dnyanesh", 20, "abc", "aa", 12345.67f);
		String str = emp.getString();
		System.out.println(str);
	}
	
	public static void main1(String[] args) {
		Employee emp = new Employee("Dnyanesh", 20, "abc", "aa", 12345.67f);
		emp.printRecord();
	}
}