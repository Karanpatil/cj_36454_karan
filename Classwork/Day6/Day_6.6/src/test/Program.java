package test;

class Person{
	private String name;
	private int age;

	public Person() {
		this.name = "";
		this.age = 0;
	}
	
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public void printRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
}

class Employee extends Person{
	private int empid;
	private float salary;

	public Employee() {
		this.empid = 0;
		this.salary = 0;
	}
	
	public Employee(String name, int age, int empid, float salary){
		super( name, age );
		this.empid = empid;
		this.salary = salary;
	}
	
	public void printRecord( ) {
		super.printRecord();
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}

public class Program {

	public static void main(String[] args) {
		Person p = new Person();
		Employee emp = (Employee) p; //Down casting : ClassCastException
	}
	
	public static void main5(String[] args) {
		Person p = new Employee();	//Up casting
		Employee emp = (Employee) p; //Down casting : OK
	}
	
	public static void main4(String[] args) {
		Person p = null;
		Employee emp = (Employee) p; //Down casting : OK
		
		System.out.println(p);	//null
		System.out.println(emp);	//null
	}
	
	public static void main3(String[] args) {
		Person p1 = new Person("Sandeep",36);
		p1.printRecord();
		
		Person p2 = p1;	//Shallow Copy of references
		p2.printRecord();
	}
	
	public static void main2(String[] args) {
		Employee emp = new Employee("Sandeep",36,33,45000.50f);
		//Person p = ( Employee ) emp;	//Up casting : OK
		Person p = emp;	//Up casting : OK
	}
	
	public static void main1(String[] args) {
		Employee emp1 = new Employee("Sandeep",36,33,45000.50f);
		emp1.printRecord();
		
		System.out.println();
		
		Employee emp2 = emp1;	//Shallow Copy of references
		emp2.printRecord();		
	}
}
