package test;

public class Program {
	
	public static void sum( int... arguments ) {
		int result = 0;
		for( int element : arguments )
			result = result + element;
		System.out.println("Result	:	"+result);
	}
	
	public static void main(String[] args) {
		Integer n1 = new Integer("125");
		//int n2 = n1.intValue();	//UnBoxing
	
		int n3 = n1;	//Auto-UnBoxing
		//int n3 = n1.intValue();		
		System.out.println(n3);
	}
	
	public static void main2(String[] args) {
		Program.sum( );
		Program.sum( 0 );
		Program.sum( 10, 20 );
		Program.sum( 10, 20, 30, 40, 50  );
		Program.sum( 10, 20, 30, 40, 50, 60, 70, 80  );
	}
	
	public static void main1(String[] args) {
		System.out.printf("Hello World\n");
		
		String str = "Sandeep";
		System.out.printf("Name : %s\n", str);
		
		int empid = 33;
		float salary = 45000.50f;
		System.out.printf("%-10s%-5d%-8.2f\n", str, empid, salary);
	}
}
