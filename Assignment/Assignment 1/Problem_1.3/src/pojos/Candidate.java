package pojos;

public class Candidate {
	private int id, votes;
	private String name, party;
	public Candidate() {
		// TODO Auto-generated constructor stub
	}
	public Candidate(int id, String name, String party, int votes) {
		super();
		this.id = id;
		this.votes = votes;
		this.name = name;
		this.party = party;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVotes() {
		return votes;
	}
	public void setVotes(int votes) {
		this.votes = votes;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParty() {
		return party;
	}
	public void setParty(String party) {
		this.party = party;
	}
	@Override
	public String toString() {
		return "Candidate [id=" + id + ", votes=" + votes + ", name=" + name + ", party=" + party + "]";
	}
	
}
