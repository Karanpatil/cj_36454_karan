package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.VoterDaoImpl;
import pojos.Voter;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/authenticate")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VoterDaoImpl dao;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			dao = new VoterDaoImpl();
		} catch (Exception e) {
			throw new ServletException("error in init of" + getClass().getName(), e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
			dao.cleanUp();
		} catch (Exception e) {
			throw new RuntimeException("in destroy of" + getClass().getName(), e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter pw = response.getWriter()){
			String email = request.getParameter("em");
			String password = request.getParameter("pass");
			Voter voter = dao.validateUser(email, password);
			if(voter == null)
				pw.print("<h5>Invalid Login , Please <a href='login.html'>Retry</a></h5>");
			else {
				Cookie c1 = new Cookie("voter_dtls", voter.toString());
				response.addCookie(c1);
				if(voter.getRole().equals("admin"))
					response.sendRedirect("adminStatus");
				else {
					if(voter.isStatus())
						response.sendRedirect("voterStatus");
					else
						response.sendRedirect("candidateList");
				}
			}
		}
		catch (Exception e) {
			throw new ServletException("error in do-post of " + getClass().getName(), e);
		}
	}

}
