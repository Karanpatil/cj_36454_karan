package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Candidate;

/**
 * Servlet implementation class CandidateServlet
 */
@WebServlet("/candidateList")
public class CandidateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CandidateDaoImpl dao;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter pw = response.getWriter()){
			Cookie[] cookies = request.getCookies();
			dao = new CandidateDaoImpl();
			List<Candidate> candidates = dao.listCandidates();
			for (Candidate candidate : candidates) {
				pw.print(candidate);
			}
		} catch (Exception e) {
			throw new ServletException("error in do-get of " + getClass().getName(), e);
		}

	}

}
