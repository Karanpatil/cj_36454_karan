package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import pojos.Voter;

public class VoterDaoImpl implements IVoterDao {
	private Connection cn;
	private PreparedStatement pst1;
	
	public VoterDaoImpl() throws Exception {
		cn = utils.DBUtils.fetchConnection();
		pst1 = cn.prepareStatement("select * from voters where email=? and password=?");
		System.out.println("Voter dao created...");
	}
	
	@Override
	public Voter validateUser(String email, String pwd) throws Exception {
		pst1.setString(1, email);
		pst1.setString(2, pwd);
		try(ResultSet rs = pst1.executeQuery()){
			if(rs.next())
				return new Voter(rs.getInt(1), rs.getString(2), email, pwd, rs.getBoolean(5), rs.getString(6));
		}
		return null;
	}
	
	public void cleanUp() throws Exception {
		if(pst1 != null)
			pst1.close();
		if(cn != null)
			cn.close();
		System.out.println("Voter dao cleaned up...");
	}
}
