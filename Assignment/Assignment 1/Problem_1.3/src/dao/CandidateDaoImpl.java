package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pojos.Candidate;

public class CandidateDaoImpl implements ICandidateDao {
	private Connection cn;
	private PreparedStatement pst1;

	public CandidateDaoImpl() throws Exception{
		cn = utils.DBUtils.fetchConnection();
		pst1 = cn.prepareStatement("select * from candidates");
		System.out.println("Candidate dao created...");
	}
	@Override
	public List<Candidate> listCandidates() throws Exception {
		List<Candidate> candidateList = new ArrayList<>();
		try(ResultSet rs = pst1.executeQuery()){
			if(rs.next()) 
				candidateList.add(new Candidate(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
			return candidateList;
		}
	}
	public void cleanUp() throws Exception {
		if(pst1 != null)
			pst1.close();
		if(cn != null)
			cn.close();
		System.out.println("Candidate dao cleaned up...");
	}
}
