package dao;

import pojos.Voter;

public interface IVoterDao {
	Voter validateUser(String email,String pwd) throws Exception;
}
