package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InputServlet
 */
@WebServlet("/test_input")
public class InputServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		System.out.println("in init of " + getClass().getName() + Thread.currentThread());
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		System.out.println("in destroy of " + getClass().getName() + Thread.currentThread());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("in do-get of " + getClass().getName() + Thread.currentThread());
		response.setContentType("text/html");
		try(PrintWriter pw = response.getWriter()){
			pw.print("<h4>User name : " + request.getParameter("f1") + "</h4>");
			String[] colors = request.getParameterValues("clr");
			pw.print("<h4>Fovourite colors : </h4>" );
			for (String color : colors) {
				pw.print("<h4>" + color + " </h4>");
			}
			pw.print("<h4>Chosen browser : " + request.getParameter("browser") + "</h4>");
			pw.print("<h4>Selected city : " + request.getParameter("myselect") + "</h4>");
			pw.print("<h4>Client information : " + request.getParameter("info") + "</h4>");
		}
	}

}
